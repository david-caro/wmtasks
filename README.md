# WMTasks
Small cli to manage phabricator tasks with opinionated defaults and workflows
from the Wikimedia phabricator service.

# Building
```
$ cargo bulid
```


# Running
```
$ cargo run -- --help
```


# Congfiguring
Currently uses the configuration from ~/.config/wmtasks/config file, for
example:
```
$ cat ~/.config/wmtasks/config
{
  "host": {
    "url": "https://phabricator.wikimedia.org/api",
    "token": "your_arc_api_token_maybe_from_.arcrc"
  },
  "team_project_tag": "wmcs-kanban",
  "personal_project_tag": "User-dcaro",
  "searches": {
    "assigned": "assigned",
    "authored": "authored",
    "subscribed": "subscribed",
    "unowned_sprint": "oX6130vti4X9",
    "resolved_by_me": "SH5VKrDBlEU3",
    "quotas": "ukoYCYFmo3y1",
    "ubn": "AtbLhf4ZUBGj",
    "vps_quota_requests": "YT8TgtnPR10c",
    "data_services_quota_requests": "8lZV61mV6HHH",
    "toolforge_quota_requests": "hzBI_HSgSKuF",
    "all_requests": "ukoYCYFmo3y1",
    "vps_project_requests": "HrdO1yrXGUYe"
  },
  "team_project_columns": {
    "inbox": "PHID-PCOL-ujqvp2iojpdziscqgmot",
    "soon": "PHID-PCOL-jpsmv6imu7iuwvnurwn4",
    "doing": "PHID-PCOL-bftyx7dc3hykv3kfdje6",
    "clinic_duty": "PHID-PCOL-zrlc6mzvvgy3flb46rik",
    "needs_discussion": "PHID-PCOL-bvud2nsvebx5bzor33ya",
    "blocked": "PHID-PCOL-ours7eel6zgm7q6ntgz2",
    "watching": "PHID-PCOL-jzv3oan67cbmvxh3wvxu",
    "graveyard": "PHID-PCOL-nxnitcod7n5cav7zqh7t",
    "epics": "PHID-PCOL-uxiikmr6hcyfv2vq2kkn"
  },
  "personal_project_columns": {
    "dbacklog": "PHID-PCOL-p52jb3memyupuqekqky6",
    "drefined": "PHID-PCOL-6gfzbyo6ks6uqfu4jajj",
    "dtoday": "PHID-PCOL-2z2u2jviw7inxiwsbtng",
    "dblocked": "PHID-PCOL-hhbwzuqachpbbtl6u5j",
    "ddoing": "PHID-PCOL-rx4rphtcx3dlsed6zyp2",
    "ddone": "PHID-PCOL-aw7pwsf3kemqdnsftsvp",
    "dinputgiven": "PHID-PCOL-nejjm64pmslwucz7ddbk"
  },
  "in_progress_column": "Doing",
  "cache_path": "~/.config/wmtasks/cache"
}
```

To get your token you have to go to 'Settings' -> 'Conduit API Tokens' while
logged in in the phabricator web UI.
