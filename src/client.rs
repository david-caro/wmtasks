use anyhow::Context;
use serde_json::from_str;
use shellexpand::tilde;
use std::collections::HashMap;
use std::path::Path;

use super::config;
use super::maniphest;
use super::models;
use futures::stream::{self, StreamExt};

pub struct ManiphestClient<'a> {
    user_cache: HashMap<String, models::User>,
    user_file_path: String,
    task_phid_to_id_cache: HashMap<String, String>,
    task_phid_to_id_file_path: String,
    phid_to_name_cache: HashMap<String, String>,
    phid_to_name_file_path: String,
    name_to_phid_cache: HashMap<String, String>,
    name_to_phid_file_path: String,
    pub config: &'a config::TasksConf,
}

pub fn ensure_tag(maybe_tag: String) -> String {
    match maybe_tag {
        tag if tag.starts_with("#") => tag,
        _ => format!("#{}", maybe_tag),
    }
}

impl<'a> ManiphestClient<'a> {
    pub fn new(config: &'a config::TasksConf) -> Self {
        let expanded_path: String = tilde(&config.cache_path).into_owned();

        let task_phid_to_id_cache_file_path: String =
            format!("{}/task_phid_to_id.json", expanded_path);
        let task_phid_to_id_path = Path::new(&task_phid_to_id_cache_file_path);
        let task_phid_to_id_cache: HashMap<String, String> = {
            if task_phid_to_id_path.exists() {
                std::fs::create_dir_all(task_phid_to_id_path.parent().unwrap()).unwrap_or(());
                let raw_config = std::fs::read_to_string(task_phid_to_id_cache_file_path.clone())
                    .with_context(|| {
                        format!("Could not read file `{}`", task_phid_to_id_cache_file_path)
                    })
                    .unwrap();
                from_str(&raw_config.to_string()).expect("Task phid to id cache JSON malformed.")
            } else {
                HashMap::new()
            }
        };

        let user_cache_file_path: String = format!("{}/users.json", expanded_path);
        let user_path = Path::new(&user_cache_file_path);
        let user_cache: HashMap<String, models::User> = {
            if user_path.exists() {
                std::fs::create_dir_all(user_path.parent().unwrap()).unwrap_or(());
                let raw_config = std::fs::read_to_string(user_cache_file_path.clone())
                    .with_context(|| format!("Could not read file `{}`", user_cache_file_path))
                    .unwrap();
                from_str(&raw_config.to_string()).expect("Users cache JSON malformed.")
            } else {
                HashMap::new()
            }
        };

        let phid_to_name_file_path: String = format!("{}/phid_to_name.json", expanded_path);
        let phid_to_name_path = Path::new(&phid_to_name_file_path);
        let phid_to_name_cache: HashMap<String, String> = {
            if phid_to_name_path.exists() {
                std::fs::create_dir_all(phid_to_name_path.parent().unwrap()).unwrap_or(());
                let raw_config = std::fs::read_to_string(phid_to_name_file_path.clone())
                    .with_context(|| format!("Could not read file `{}`", phid_to_name_file_path))
                    .unwrap();
                from_str(&raw_config.to_string()).expect("Task phid to id cache JSON malformed.")
            } else {
                HashMap::new()
            }
        };

        let name_to_phid_file_path: String = format!("{}/name_to_phid.json", expanded_path);
        let name_to_phid_path = Path::new(&name_to_phid_file_path);
        let name_to_phid_cache: HashMap<String, String> = {
            if name_to_phid_path.exists() {
                std::fs::create_dir_all(name_to_phid_path.parent().unwrap()).unwrap_or(());
                let raw_config = std::fs::read_to_string(name_to_phid_file_path.clone())
                    .with_context(|| format!("Could not read file `{}`", name_to_phid_file_path))
                    .unwrap();
                from_str(&raw_config.to_string()).expect("Task phid to id cache JSON malformed.")
            } else {
                HashMap::new()
            }
        };

        Self {
            user_cache,
            config,
            task_phid_to_id_cache,
            phid_to_name_cache,
            name_to_phid_cache,
            task_phid_to_id_file_path: task_phid_to_id_cache_file_path,
            phid_to_name_file_path: phid_to_name_file_path,
            name_to_phid_file_path: name_to_phid_file_path,
            user_file_path: user_cache_file_path,
        }
    }

    pub async fn get_task_id<'b>(&mut self, phid: String) -> models::MaybeResult<String> {
        if !self.task_phid_to_id_cache.contains_key(&phid) {
            self.task_phid_to_id_cache.insert(
                phid.to_owned(),
                match maniphest::get_task_details_by_phid(self.config, &phid)
                    .await
                    .expect("Got error trying to get task details")
                    .id
                {
                    Some(task_id) => format!("T{}", task_id),
                    None => panic!("Unable to get id for task with phid {}", phid),
                },
            );
        };
        std::fs::write(
            self.task_phid_to_id_file_path.clone(),
            serde_json::to_string(&self.task_phid_to_id_cache).unwrap(),
        )?;
        Ok(self.task_phid_to_id_cache.get(&phid).unwrap().clone())
    }

    pub async fn get_user<'b>(&mut self, phid: &String) -> models::MaybeResult<models::User> {
        if !self.user_cache.contains_key(phid) {
            self.user_cache.insert(
                phid.to_owned(),
                maniphest::get_user(self.config, phid)
                    .await
                    .expect("Got error trying to get user"),
            );
        };
        std::fs::write(
            self.user_file_path.clone(),
            serde_json::to_string(&self.user_cache).unwrap(),
        )?;
        Ok(self.user_cache.get(phid).unwrap().to_owned())
    }

    pub async fn get_task_details(&self, task_id: &String) -> models::MaybeResult<models::Task> {
        maniphest::get_task_details_by_id(&self.config, task_id).await
    }

    pub async fn get_task_comments(
        &self,
        task_id: &String,
    ) -> models::MaybeResult<Vec<maniphest::Comment>> {
        maniphest::get_task_comments(&self.config, task_id).await
    }

    pub async fn get_board(
        &'a self,
        project_tag: &String,
    ) -> models::MaybeResult<HashMap<String, Vec<models::Task>>> {
        let mut boards: HashMap<String, Vec<models::Task>> = HashMap::new();
        let resolved_project_tag = match project_tag.as_str() {
            "get_from_config" => &self.config.personal_project_tag,
            "personal" => &self.config.personal_project_tag,
            "team" => &self.config.team_project_tag,
            _ => project_tag,
        };
        let project_phid = {
            if resolved_project_tag.starts_with("PHID-") {
                resolved_project_tag.clone()
            } else {
                ManiphestClient::new(&self.config)
                    .get_phid_from_name(&ensure_tag(resolved_project_tag.clone()).to_string())
                    .await
                    .unwrap()
            }
        };
        let project_phid_str = project_phid.as_str();
        let tasks = maniphest::get_tasks_for_project(&self.config, resolved_project_tag).await?;
        for task in tasks {
            let _results = stream::iter(match task.attachments.columns.clone().unwrap().boards {
                models::Boards::Boards(gotten_boards) => gotten_boards,
                models::Boards::Value(_) => HashMap::new(),
            })
            .then(|item| async move {
                match Some(item.0) {
                    Some(phid) if phid.as_str() == project_phid_str => item
                        .1
                        .columns
                        .iter()
                        .map(|column| column.name.clone())
                        .collect::<Vec<String>>(),
                    _ => Vec::new(),
                }
            })
            .collect::<Vec<Vec<String>>>()
            .await;

            for column_name in _results.iter().flatten() {
                boards
                    .entry(column_name.to_owned())
                    .or_insert(Vec::new())
                    .push(task.clone());
            }
        }
        Ok(boards)
    }

    pub async fn get_tasks_by_saved_search(
        &self,
        saved_search: &String,
    ) -> models::MaybeResult<Vec<models::Task>> {
        maniphest::get_tasks_by_saved_search(&self.config, saved_search).await
    }

    pub async fn search_tasks(&self, query: &String) -> models::MaybeResult<Vec<models::Task>> {
        maniphest::search_tasks(&self.config, query).await
    }

    pub async fn search_users(
        &self,
        query: &String,
    ) -> models::MaybeResult<Vec<maniphest::UserSearchEntry>> {
        maniphest::search_users(&self.config, query).await
    }

    pub async fn comment_on_task(
        &self,
        task_id: &String,
        comment: &String,
    ) -> models::MaybeResult<String> {
        Ok(maniphest::comment_on_task(&self.config, task_id, comment)
            .await
            .unwrap()
            .object
            .id
            .to_string())
    }

    pub async fn create_task(
        &self,
        title: &String,
        description: &String,
        parent: Option<String>,
        claim: bool,
        priority: Option<String>,
        origin: String,
        worktype: String,
    ) -> models::MaybeResult<String> {
        Ok(maniphest::create_task(
            &self.config,
            title,
            description,
            parent,
            claim,
            priority,
            origin,
            worktype,
        )
        .await
        .unwrap()
        .object
        .id
        .to_string())
    }

    pub async fn close_task(&self, task_id: &String) -> models::MaybeResult<String> {
        self.set_status(task_id, models::TaskStatus::Resolved).await
    }

    pub async fn claim_task(&self, task_id: &String) -> models::MaybeResult<String> {
        Ok(maniphest::claim_task(&self.config, task_id)
            .await
            .unwrap()
            .object
            .id
            .to_string())
    }

    pub async fn subscribe(&self, task_id: &String) -> models::MaybeResult<String> {
        Ok(maniphest::subscribe(&self.config, task_id)
            .await
            .unwrap()
            .object
            .id
            .to_string())
    }

    pub async fn tag(&self, task_id: &String, tag_phid: &String) -> models::MaybeResult<String> {
        Ok(maniphest::tag(&self.config, task_id, tag_phid)
            .await
            .unwrap()
            .object
            .id
            .to_string())
    }

    pub async fn untag(&self, task_id: &String, tag_phid: &String) -> models::MaybeResult<String> {
        Ok(maniphest::untag(&self.config, task_id, tag_phid)
            .await
            .unwrap()
            .object
            .id
            .to_string())
    }

    pub async fn unsubscribe(&self, task_id: &String) -> models::MaybeResult<String> {
        Ok(maniphest::unsubscribe(&self.config, task_id)
            .await
            .unwrap()
            .object
            .id
            .to_string())
    }

    pub async fn change_priority(
        &self,
        task_id: &String,
        new_priority: models::TaskPriority,
    ) -> models::MaybeResult<String> {
        Ok(
            maniphest::change_priority(&self.config, task_id, new_priority)
                .await
                .unwrap()
                .object
                .id
                .to_string(),
        )
    }

    pub async fn set_status(
        &self,
        task_id: &String,
        new_status: models::TaskStatus,
    ) -> models::MaybeResult<String> {
        Ok(maniphest::set_status(&self.config, task_id, new_status)
            .await
            .unwrap()
            .object
            .id
            .to_string())
    }

    pub async fn change_title(
        &self,
        task_id: &String,
        new_title: &String,
    ) -> models::MaybeResult<String> {
        Ok(maniphest::change_title(&self.config, task_id, new_title)
            .await
            .unwrap()
            .object
            .id
            .to_string())
    }

    pub async fn change_description(
        &self,
        task_id: &String,
        new_description: &String,
    ) -> models::MaybeResult<String> {
        Ok(
            maniphest::change_description(&self.config, task_id, new_description)
                .await
                .unwrap()
                .object
                .id
                .to_string(),
        )
    }

    pub async fn move_to(
        &self,
        task_id: &String,
        new_column: models::BoardColumn,
    ) -> models::MaybeResult<()> {
        maniphest::move_to(&self.config, task_id, new_column)
            .await
            .unwrap();
        Ok(())
    }

    pub async fn remove_parent(
        &self,
        child_task_id: &String,
        parent_task_id: &String,
    ) -> models::MaybeResult<()> {
        maniphest::remove_parent(&self.config, child_task_id, parent_task_id)
            .await
            .unwrap();
        Ok(())
    }

    pub async fn add_parent(
        &self,
        child_task_id: &String,
        parent_task_id: &String,
    ) -> models::MaybeResult<()> {
        maniphest::add_parent(&self.config, child_task_id, parent_task_id)
            .await
            .unwrap();
        Ok(())
    }

    pub async fn add_subscriber(
        &self,
        task_id: &String,
        user_name: &String,
    ) -> models::MaybeResult<()> {
        maniphest::add_subscriber(&self.config, task_id, user_name)
            .await
            .unwrap();
        Ok(())
    }

    pub async fn remove_subscriber(
        &self,
        task_id: &String,
        user_name: &String,
    ) -> models::MaybeResult<()> {
        maniphest::remove_subscriber(&self.config, task_id, user_name)
            .await
            .unwrap();
        Ok(())
    }

    pub async fn assign(&self, task_id: &String, user_name: &String) -> models::MaybeResult<()> {
        maniphest::assign(&self.config, task_id, user_name)
            .await
            .unwrap();
        Ok(())
    }

    pub async fn unassign(&self, task_id: &String) -> models::MaybeResult<()> {
        maniphest::unassign(&self.config, task_id).await.unwrap();
        Ok(())
    }

    pub async fn get_parent_and_children_ids(
        &mut self,
        task_id: &String,
    ) -> (Vec<String>, Vec<String>) {
        let familiars = maniphest::get_parent_and_children(&self.config, task_id)
            .await
            .unwrap();
        let mut parents = Vec::new();
        let mut children = Vec::new();
        for familiar in familiars {
            match familiar.edge_type.as_str() {
                "task.subtask" => {
                    children.push(self.get_task_id(familiar.destination_phid).await.unwrap())
                }
                "task.parent" => {
                    parents.push(self.get_task_id(familiar.destination_phid).await.unwrap())
                }
                other => panic!("Unknown edge relation for parent/children: {}", other),
            }
        }
        (parents, children)
    }

    pub async fn get_name_from_phid<'b>(&mut self, phid: &String) -> models::MaybeResult<String> {
        if !self.phid_to_name_cache.contains_key(phid) {
            self.phid_to_name_cache.insert(
                phid.to_owned(),
                maniphest::name_lookup(self.config, phid)
                    .await
                    .expect("Got error trying to get name from phid"),
            );
        };
        std::fs::write(
            self.phid_to_name_file_path.clone(),
            serde_json::to_string(&self.phid_to_name_cache).unwrap(),
        )?;
        Ok(self.phid_to_name_cache.get(phid).unwrap().to_owned())
    }

    pub async fn get_phid_from_name<'b>(&mut self, name: &String) -> models::MaybeResult<String> {
        if !self.name_to_phid_cache.contains_key(name) {
            match maniphest::phid_lookup(self.config, name).await {
                Ok(phid) => self.name_to_phid_cache.insert(name.to_owned(), phid),
                Err(e) => return Err(e),
            };
        };
        std::fs::write(
            self.name_to_phid_file_path.clone(),
            serde_json::to_string(&self.name_to_phid_cache).unwrap(),
        )?;
        Ok(self.name_to_phid_cache.get(name).unwrap().to_owned())
    }

    pub async fn phaste<'b>(&mut self, text: &String) -> models::MaybeResult<String> {
        Ok(maniphest::phaste(self.config, text).await.unwrap())
    }
}
