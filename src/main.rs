use anyhow::Context;
use anyhow::Result;
use chrono::prelude::DateTime;
use chrono::Local;
use clap::Parser;
use colored::*;
use serde_json::from_str;
use shellexpand::tilde;
use std::collections::HashMap;
use std::error::Error;
use std::fmt::Write as FmtWrite;
use std::io::{self, Read, Write};
use std::path::Path;
use std::str::FromStr;
use std::time::{Duration, UNIX_EPOCH};

use futures::stream::{self, StreamExt};
use itertools::sorted;
use tabular::{Row, Table};

mod client;
mod config;
mod editor;
mod maniphest;
mod models;

/*
Looking up for a phid:
$ echo '{"names": ["#cloud-services-team"]}' | arc call-conduit phid.lookup --
{
"error": null,
"errorMessage": null,
"response": {
"#cloud-services-team": {
"phid": "PHID-PROJ-d3h5oaspnvdwfjklmivl",
"uri": "https://phabricator.wikimedia.org/tag/cloud-services-team/",
"typeName": "Project",
"type": "PROJ",
"name": "cloud-services-team",
"fullName": "cloud-services-team",
"status": "open"
}
}
}
*/

// test Show/list/create/close tasks from maniphest (phabricator), with wikimedia
// (and my personal) specific tweaks.
#[derive(Parser)]
#[clap(version = "1.0", author = "David Caro <me@dcaro.es>")]
struct Opts {
    #[clap(short, long, default_value = "~/.config/wmtasks/config")]
    config: String,

    #[clap(short, long)]
    no_color: bool,

    #[clap(subcommand)]
    subcommand: SubCommand,
}

#[derive(Parser)]
enum SubCommand {
    AddComment(AddComment),
    AddParent(AddParent),
    AddSubscriber(AddSubscriber),
    Assign(Assign),
    Backlog(Backlog),
    Block(Block),
    ChangePriority(ChangePriority),
    Claim(Claim),
    Close(Close),
    Create(Create),
    Doing(Doing),
    Done(Done),
    EditDescription(EditDescription),
    ForToday(ForToday),
    GetName(GetName),
    GetPHID(GetPHID),
    Handle(Handle),
    List(List),
    ListSavedSearches(ListSavedSearches),
    MoveTo(MoveTo),
    Open(Open),
    Refine(Refine),
    RemoveParent(RemoveParent),
    RemoveSubscriber(RemoveSubscriber),
    Search(Search),
    SearchUsers(SearchUsers),
    SetStatus(SetStatus),
    SetTitle(SetTitle),
    Show(Show),
    ShowBoard(ShowBoard),
    Subscribe(Subscribe),
    Tag(Tag),
    Unassign(Unassign),
    Unhandle(Unhandle),
    Unsubscribe(Unsubscribe),
    Untag(Untag),
    Phaste(Phaste),
}

/// List the tasks contained in a given query id/name
#[derive(Parser)]
struct List {
    #[clap(short, long)]
    debug: bool,

    #[clap(default_value = "assigned")]
    saved_search: String,

    #[clap(short, long)]
    only_ids: bool,
}

/// List the configured aliased searches
#[derive(Parser)]
struct ListSavedSearches {
    #[clap(short, long)]
    debug: bool,
}

/// Show the details of a specific task
#[derive(Parser)]
struct Show {
    #[clap(short, long)]
    debug: bool,

    #[clap(long)]
    with_phids: bool,

    #[clap(short, long)]
    without_comments: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Show a kanban board ("default|personal" -> configured personal project, "team" -> configured team project, _ -> that project tag)
#[derive(Parser)]
struct ShowBoard {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long, default_value = "all")]
    column: String,

    #[clap(short, long, default_value = "get_from_config")]
    project_tag: String,

    #[clap(long)]
    with_phids: bool,
}

/// Move a task to doing
#[derive(Parser)]
struct Doing {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Add the given comment to the given task, if you pass a single 'e' as comment, an editor will
/// open up for you to write it.
#[derive(Parser)]
struct AddComment {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(short, long)]
    comment: Option<String>,
}

/// Add the given task as a parent of another (making the latter a subtask)
#[derive(Parser)]
struct AddParent {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    child_task_id: Option<String>,

    #[clap(short, long)]
    parent_task_id: String,
}

/// Subscribe the given user to the task
#[derive(Parser)]
struct AddSubscriber {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(short, long)]
    user_name: String,
}

/// Unsusbscribe the given user from the task
#[derive(Parser)]
struct RemoveSubscriber {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(short, long)]
    user_name: String,
}

/// Assign the given task to the given user
#[derive(Parser)]
struct Assign {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(short, long)]
    user_name: String,
}

/// Unssign the given task
#[derive(Parser)]
struct Unassign {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Move a task to the blocked columnt
#[derive(Parser)]
struct Block {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}
/// Remove the given task from the parents of another if there
#[derive(Parser)]
struct RemoveParent {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    child_task_id: Option<String>,

    #[clap(short, long)]
    parent_task_id: String,
}

/// Create a task
#[derive(Parser, Debug)]
struct Create {
    #[clap(long)]
    debug: bool,

    #[clap(short, long)]
    title: String,

    #[clap(short, long)]
    parent: Option<String>,

    #[clap(long)]
    description: Option<String>,

    #[clap(short, long)]
    claim: bool,

    #[clap(long)]
    priority: Option<String>,

    #[clap(short, long, value_enum)]
    origin: maniphest::TaskOrigin,

    #[clap(short, long, value_enum)]
    worktype: maniphest::TaskWorktype,
}

/// Claim a task, uses the api token to find out the current user.
#[derive(Parser)]
struct Claim {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Subscribe yourself to a task, uses the api token to find out the current
/// user.
#[derive(Parser)]
struct Subscribe {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Add a tag to the task.
#[derive(Parser)]
struct Tag {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(long)]
    tag: String,
}

/// Remove a tag from the task.
#[derive(Parser)]
struct Untag {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(long)]
    tag: String,
}

/// Search tasks given a query.
#[derive(Parser)]
struct Search {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    query: String,

    #[clap(short, long)]
    only_ids: bool,
}

/// Search users given a query.
#[derive(Parser)]
struct SearchUsers {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    query: String,

    #[clap(short, long)]
    only_ids: bool,
}

/// Unsubscribe yourself to a task, uses the api token to find out the current
/// user.
#[derive(Parser)]
struct Unsubscribe {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Remove the personal project tag from the task.
#[derive(Parser)]
struct Unhandle {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Add the personal project tag from the task.
#[derive(Parser)]
struct Handle {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Close a task
#[derive(Parser)]
struct Close {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Move a task to the done column
#[derive(Parser)]
struct Done {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Change the priority of a task
#[derive(Parser)]
struct ChangePriority {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(short, long)]
    new_priority: String,
}

/// Change the title of a task
#[derive(Parser)]
struct SetTitle {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(short, long)]
    new_title: String,
}

/// Change the title of a task
#[derive(Parser)]
struct SetStatus {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(short, long)]
    new_status: String,
}

/// Change the description of a task
#[derive(Parser)]
struct EditDescription {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(short, long)]
    new_description: Option<String>,
}

/// Move a task to the "For today" column
#[derive(Parser)]
struct ForToday {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Given a PHID return a name
#[derive(Parser)]
struct GetName {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    phid: String,
}

/// Given a name, return it's PHID
#[derive(Parser)]
struct GetPHID {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    name: String,
}

/// Move a task to the "Refined" column
#[derive(Parser)]
struct Refine {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Move a task to the "To Refine" column
#[derive(Parser)]
struct Backlog {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Change the column of a task
#[derive(Parser)]
struct MoveTo {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,

    #[clap(short, long)]
    new_column: String,
}

/// Open the task on your browser
#[derive(Parser)]
struct Open {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    task_id: Option<String>,
}

/// Create a phaste
#[derive(Parser)]
struct Phaste {
    #[clap(short, long)]
    debug: bool,

    #[clap(short, long)]
    text: Option<String>,
}

struct DistinctColorer<'a> {
    colors: Vec<Color>,
    categories: &'a mut HashMap<String, Color>,
}

impl<'a> DistinctColorer<'a> {
    fn color(&mut self, key: String, to_color: Option<String>) -> ColoredString {
        let item = to_color.unwrap_or(key.clone());
        match self.categories.get(&key) {
            Some(color) => item.color(color.to_owned()),
            None => {
                let color = self.colors[self.categories.len() % self.colors.len()];
                self.categories.insert(key.to_owned(), color);
                item.color(color)
            }
        }
    }
}

struct DistinctColored<'a, I>
where
    I: Sized,
    I: Iterator<Item = String>,
{
    iter: I,
    colorer: DistinctColorer<'a>,
}

impl<'a, I> Iterator for DistinctColored<'a, I>
where
    I: Iterator<Item = String>,
{
    type Item = String;

    fn next(&mut self) -> Option<String> {
        match self.iter.next() {
            Some(elem) => Some(format!("{}", self.colorer.color(elem, None))),
            None => None,
        }
    }
}

trait Colorable {
    fn color_each_differently<'a>(
        self,
        categories: &'a mut HashMap<String, Color>,
    ) -> DistinctColored<Self>
    where
        Self: Sized,
        Self: Iterator<Item = String>;
}

impl<I> Colorable for I
where
    I: Iterator,
    I: Sized,
{
    fn color_each_differently<'a>(
        self,
        categories: &'a mut HashMap<String, Color>,
    ) -> DistinctColored<Self>
    where
        Self: Sized,
        Self: Iterator<Item = String>,
    {
        DistinctColored {
            iter: self,
            colorer: DistinctColorer {
                colors: vec![
                    Color::Red,
                    Color::Green,
                    Color::Yellow,
                    Color::Blue,
                    Color::Magenta,
                    Color::Cyan,
                    Color::White,
                    Color::BrightBlack,
                    Color::BrightRed,
                    Color::BrightGreen,
                    Color::BrightYellow,
                    Color::BrightBlue,
                    Color::BrightMagenta,
                    Color::BrightCyan,
                    Color::BrightWhite,
                ],
                categories,
            },
        }
    }
}

fn parse_task(conf: &config::TasksConf, maybe_task_id: Option<String>) -> String {
    match maybe_task_id {
        Some(task_id) => {
            set_last_task(&conf, &task_id);
            task_id
        }
        None => match get_last_task(&conf) {
            Some(task_id) => task_id,
            None => panic!("No previous task id found, please provide one."),
        },
    }
}

fn get_last_task(conf: &config::TasksConf) -> Option<String> {
    let expanded_path: String = tilde(&conf.cache_path).into_owned();

    let last_task_cache_str: String = format!("{}/last_task.json", expanded_path);
    let last_task_cache_path = Path::new(&last_task_cache_str);
    if last_task_cache_path.exists() {
        let raw_config = std::fs::read_to_string(last_task_cache_path.clone())
            .with_context(|| format!("Could not read file `{}`", last_task_cache_str))
            .unwrap();
        return Some(
            from_str(&raw_config.to_string()).expect("Task phid to id cache JSON malformed."),
        );
    } else {
        return None;
    }
}

fn set_last_task(conf: &config::TasksConf, task_id: &String) {
    let expanded_path: String = tilde(&conf.cache_path).into_owned();

    let last_task_cache_str: String = format!("{}/last_task.json", expanded_path);
    let last_task_cache_path = Path::new(&last_task_cache_str);
    if last_task_cache_path.exists() {
    } else {
        std::fs::create_dir_all(last_task_cache_path.parent().unwrap()).unwrap_or(());
    }
    std::fs::write(
        last_task_cache_path,
        serde_json::to_string(task_id).unwrap(),
    )
    .unwrap();
}

fn shorten_project_name(project_name: &String) -> String {
    let mut short_name: String = String::new();
    for part in project_name
        .as_str()
        .split(|c: char| (c == ' ' || c == '-' || c == ':' || c == '('))
    {
        if part != "" {
            short_name.push(part.chars().next().unwrap());
        }
    }
    return short_name;
}

fn print_searches(config: &config::TasksConf) {
    println!("{:?}", config.searches)
}

async fn print_board(
    cli: &mut client::ManiphestClient<'_>,
    board: &HashMap<String, Vec<models::Task>>,
    column: &String,
    output: &mut String,
    with_phids: bool,
) {
    let selected_columns = match column {
        column if column.contains(",") => column
            .split(",")
            .map(|colname| colname.to_lowercase())
            .collect(),
        column => vec![column.to_lowercase()],
    };
    for (board_name, tasks) in board {
        if selected_columns.contains(&String::from("all"))
            || selected_columns.contains(&board_name.to_lowercase())
        {
            writeln!(
                output,
                "{}",
                format!("######### {}", board_name).color(Color::Yellow)
            )
            .unwrap();
            print_tasks(cli, &tasks, output, with_phids).await
        }
    }
}

async fn print_users(users: &Vec<maniphest::UserSearchEntry>, output: &mut String) {
    let default_username = "no_username".to_string();
    let default_realname = "no_realname".to_string();
    for user in users {
        let username = match user.fields.get("username").unwrap() {
            maniphest::UserSearchEntryField::StringField(username) => username,
            _ => &default_username,
        };
        let realname = match user.fields.get("realName").unwrap() {
            maniphest::UserSearchEntryField::StringField(realname) => realname,
            _ => &default_realname,
        };
        writeln!(output, "{} <{}>", realname, username).unwrap();
    }
}

async fn print_tasks(
    cli: &mut client::ManiphestClient<'_>,
    tasks: &Vec<models::Task>,
    output: &mut String,
    with_phids: bool,
) {
    // first pass to retrieve all the users
    for task in tasks {
        match task.fields.get("ownerPHID") {
            Some(models::Field::String(phid)) => {
                cli.get_user(phid)
                    .await
                    .expect("Got error trying to get user");
            }
            Some(models::Field::None) => (),
            Some(other) => panic!("Invalid ownerPHID {:?}", other),
            None => (),
        }
    }

    let mut table = Table::new("{:<}{:^}:{:<}{:<}{:<}");
    let mut column_color_categories: HashMap<String, Color> = HashMap::new();
    let mut user_colorer = DistinctColorer {
        categories: &mut HashMap::new(),
        colors: vec![
            Color::Red,
            Color::Green,
            Color::Yellow,
            Color::Blue,
            Color::Magenta,
            Color::Cyan,
            Color::White,
            Color::BrightRed,
            Color::BrightGreen,
            Color::BrightYellow,
            Color::BrightBlue,
            Color::BrightMagenta,
            Color::BrightCyan,
            Color::BrightWhite,
            Color::BrightBlack,
        ],
    };
    for task in tasks {
        let d = UNIX_EPOCH
            + Duration::from_secs(match task.fields.get("dateModified").unwrap() {
                models::Field::DateModified(date_modified) => date_modified.clone(),
                _ => panic!(),
            });
        let last_modified_date = DateTime::<Local>::from(d);
        let name = match task.fields.get("name").unwrap() {
            models::Field::String(name) => name,
            _ => panic!("Malformed description"),
        };
        let owner_str: ColoredString = match task.fields.get("ownerPHID") {
            Some(models::Field::String(phid)) => {
                let user = cli
                    .get_user(phid)
                    .await
                    .expect(format!("Unknown phid {}", phid).as_str());
                user_colorer.color(
                    match with_phids {
                        true => format!("{}[{}]", user.user_name, user.phid),
                        false => user.user_name,
                    },
                    None,
                )
            }
            Some(models::Field::None) => "unassigned".white(),
            Some(other) => panic!("Invalid ownerPHID {:?}", other),
            None => "unassigned".white(),
        };
        let color: Color;
        let id_prio_str = match task.fields.get("priority").unwrap() {
            models::Field::Priority(priority) => {
                color = match priority.color.as_ref().unwrap().as_str() {
                    "pink" => Color::BrightRed,
                    "violet" => Color::Magenta,
                    "red" => Color::Red,
                    "orange" => Color::Cyan,
                    "yellow" => Color::BrightWhite,
                    "sky" => Color::BrightWhite,
                    other => {
                        writeln!(output, "{:?}", other).unwrap();
                        Color::Green
                    }
                };
                format!("T{}({}", task.id.unwrap(), &priority.name[..1]).color(color)
            }
            _ => panic!("Malformed priority"),
        };
        let status = match task.fields.get("status").unwrap() {
            models::Field::Status(status) => match status.name.as_str() {
                "Open" => format!("{}", "Op").color(Color::Green),
                "Closed" => format!("{}", "Cl").color(Color::White),
                "Resolved" => format!("{}", "Re").color(Color::White),
                "Declined" => format!("{}", "De").color(Color::Magenta),
                "Stalled" => format!("{}", "St").color(Color::Red),
                other => format!("{}", other).color(Color::Magenta),
            },
            _ => panic!("Status not found"),
        };

        let config = cli.config.clone();
        let board_columns: Vec<String> = sorted(
            stream::iter(match task.attachments.columns.clone().unwrap().boards {
                models::Boards::Boards(boards) => boards,
                models::Boards::Value(_) => HashMap::new(),
            })
            .then(|item| async move {
                let project_name = client::ManiphestClient::new(config.clone())
                    .get_name_from_phid(&item.0)
                    .await
                    .unwrap();
                item.1
                    .columns
                    .iter()
                    .map(|column| match with_phids {
                        true => format!(
                            "{}:{}<{}:{}>",
                            project_name,
                            item.0,
                            column.name.clone(),
                            column.phid.clone()
                        ),
                        false => format!(
                            "{}<{}>",
                            shorten_project_name(&project_name),
                            column.name.clone()
                        ),
                    })
                    .collect::<Vec<String>>()
            })
            .collect::<Vec<Vec<String>>>()
            .await
            .iter()
            .flatten()
            .map(|column| column.to_owned())
            .color_each_differently(&mut column_color_categories)
            .collect::<Vec<String>>(),
        );

        let projects: Vec<String> =
            stream::iter(task.attachments.projects.clone().unwrap().project_phids)
                .then(|project_phid| async move {
                    let project_name = client::ManiphestClient::new(config.clone())
                        .get_name_from_phid(&project_phid)
                        .await
                        .unwrap();
                    match project_name.as_str() {
                        "Cloud-Services-Worktype-Project" => "wp".color(Color::Blue).to_string(),
                        "Cloud-Services-Worktype-Unplanned" => "wu".color(Color::Green).to_string(),
                        "Cloud-Services-Worktype-Maintenance" => {
                            "wm".color(Color::Cyan).to_string()
                        }
                        "Cloud-Services-Origin-User" => "ou".color(Color::Magenta).to_string(),
                        "Cloud-Services-Origin-Alert" => "oa".color(Color::Yellow).to_string(),
                        "Cloud-Services-Origin-Team" => {
                            "ot".color(Color::BrightMagenta).to_string()
                        }
                        _ => "".to_string(),
                    }
                })
                .collect::<Vec<String>>()
                .await;
        let colored_projects: Vec<String> = sorted(
            projects
                .iter()
                .map(|project| project.to_owned())
                .filter(|project| project.to_owned().len() > 0)
                .collect::<Vec<String>>(),
        );
        table.add_row(
            Row::new()
                .with_cell(last_modified_date.format("%Y-%m-%d %H:%M:%S ").to_string())
                .with_cell(owner_str)
                .with_cell(id_prio_str)
                .with_cell(format!("{}{}", status, ")".color(color)))
                .with_cell(format!(
                    "[{}]{{{}}}{}",
                    colored_projects.join(","),
                    board_columns.join(","),
                    name.color(color)
                )),
        );
    }

    writeln!(output, "{}", table).unwrap()
}

fn open_pager(input: &String) {
    let mut pager = std::process::Command::new("less")
        .stdin(std::process::Stdio::piped())
        .arg("-RFS")
        .spawn()
        .expect("command failed");
    {
        // limited borrow of stdin
        let stdin = pager.stdin.as_mut().expect("failed to get stdin");
        stdin
            .write_all(input.as_bytes())
            .expect("failed to write to stdin");
    }

    pager.wait().expect("wait failed");
}

fn open_browser(url: &String) {
    let mut opener = std::process::Command::new("xdg-open")
        .arg(url)
        .spawn()
        .expect("command failed");

    opener.wait().expect("wait failed");
}

async fn print_task_details(
    cli: &mut client::ManiphestClient<'_>,
    task: &models::Task,
    comments: &Vec<maniphest::Comment>,
    output: &mut String,
    with_phids: bool,
) {
    let author: models::User = cli
        .get_user(match task.fields.get("authorPHID").unwrap() {
            models::Field::String(phid) => phid,
            _ => panic!("Unable to find authorPHID"),
        })
        .await
        .unwrap();
    let owner: Option<models::User> = match task.fields.get("ownerPHID").unwrap() {
        models::Field::String(phid) => Some(cli.get_user(&phid).await.unwrap()),
        _ => None,
    };
    let mut subscribers: Vec<models::User> = Vec::new();
    for phid in task
        .attachments
        .subscribers
        .clone()
        .unwrap()
        .subscriber_phids
        .iter()
    {
        subscribers.push(cli.get_user(&phid).await.unwrap());
    }
    writeln!(
        output,
        "{}",
        format!(
            "[T{}] {}",
            task.id.unwrap().to_string(),
            match task.fields.get("name").unwrap() {
                models::Field::String(title) => title,
                _ => panic!("Title not found"),
            }
        )
        .color(Color::Yellow)
    )
    .unwrap();
    let d = UNIX_EPOCH
        + Duration::from_secs(match task.fields.get("dateModified").unwrap() {
            models::Field::DateModified(date_modified) => date_modified.clone(),
            _ => panic!(),
        });
    let last_modified_date = DateTime::<Local>::from(d);
    writeln!(
        output,
        "{}\t{}\t{}",
        format!(
            "Status({})",
            match task.fields.get("status").unwrap() {
                models::Field::Status(status) => status.name.clone(),
                _ => panic!("Status not found"),
            }
        )
        .color(Color::Yellow),
        format!(
            "Priority({})",
            match task.fields.get("priority").unwrap() {
                models::Field::Priority(priority) => priority.name.clone(),
                _ => panic!("Status not found"),
            }
        )
        .color(Color::Yellow),
        format!(
            "Last modified: {}",
            last_modified_date
                .format("%Y-%m-%d %H:%M:%S.%f")
                .to_string()
        )
        .color(Color::Yellow)
    )
    .unwrap();
    writeln!(
        output,
        "{}\t{}",
        format!(
            "Owner: {}",
            match owner {
                Some(owner) => {
                    if with_phids {
                        let phid = owner.phid.clone();
                        format!("{}[{}]", owner.to_string(), phid)
                    } else {
                        owner.to_string()
                    }
                }
                None => "Unassigned".to_string(),
            },
        )
        .color(Color::Green),
        format!(
            "Author: {}",
            match with_phids {
                false => author.to_string(),
                true => {
                    let phid = author.phid.clone();
                    format!("{}[{}]", author.to_string(), phid)
                }
            }
        )
        .color(Color::Green),
    )
    .unwrap();

    match task.id {
        Some(task_id) => {
            let parents_and_children = cli
                .get_parent_and_children_ids(&format!("T{}", task_id))
                .await;
            writeln!(
                output,
                "{}\n{}",
                format!("Parent tasks: {}", parents_and_children.0.join(", ")).color(Color::Green),
                format!("Children tasks: {}", parents_and_children.1.join(", "))
                    .color(Color::Green)
            )
            .unwrap()
        }
        None => (),
    };

    let config = cli.config.clone();
    let mut project_color_categories: HashMap<String, Color> = HashMap::new();

    let projects: Vec<String> =
        stream::iter(task.attachments.projects.clone().unwrap().project_phids)
            .then(|project_phid| async move {
                let project_name = client::ManiphestClient::new(config.clone())
                    .get_name_from_phid(&project_phid)
                    .await
                    .unwrap();
                match with_phids {
                    false => project_name,
                    true => format!("{}:{}", project_name, project_phid),
                }
            })
            .collect::<Vec<String>>()
            .await;
    let colored_projects: Vec<String> = projects
        .iter()
        .map(|project| project.to_owned())
        .filter(|project| project.to_owned().len() > 0)
        .color_each_differently(&mut project_color_categories)
        .collect::<Vec<String>>();
    writeln!(output, "Projects: {}", colored_projects.join(", ")).unwrap();

    let board_columns: Vec<String> =
        stream::iter(match task.attachments.columns.clone().unwrap().boards {
            models::Boards::Boards(boards) => boards,
            models::Boards::Value(_) => HashMap::new(),
        })
        .then(|item| async move {
            let project_name = client::ManiphestClient::new(config.clone())
                .get_name_from_phid(&item.0)
                .await
                .unwrap();
            item.1
                .columns
                .iter()
                .map(|column| {
                    if with_phids {
                        format!(
                            "{}:{}<{}:{}>",
                            project_name,
                            item.0,
                            column.name.clone(),
                            column.phid.clone()
                        )
                    } else {
                        format!(
                            "{}<{}>",
                            shorten_project_name(&project_name),
                            column.name.clone()
                        )
                    }
                })
                .collect::<Vec<String>>()
        })
        .collect::<Vec<Vec<String>>>()
        .await
        .iter()
        .flatten()
        .map(|column| column.to_owned())
        .color_each_differently(&mut HashMap::new())
        .collect::<Vec<String>>();
    writeln!(output, "Columns: {}", board_columns.join(", ")).unwrap();

    writeln!(
        output,
        "{}",
        format!(
            "cc: {}",
            subscribers
                .iter()
                .map(|user| {
                    if with_phids {
                        let phid = user.phid.clone();
                        format!("{}[{}]", user.to_owned().to_string(), phid)
                    } else {
                        user.to_owned().to_string()
                    }
                })
                .collect::<Vec<String>>()
                .join(", ")
        )
        .color(Color::Magenta)
    )
    .unwrap();
    writeln!(
        output,
        "-------------------------------------------------------------"
    )
    .unwrap();
    writeln!(
        output,
        "Description:\n{}",
        match task.fields.get("description").unwrap() {
            models::Field::Description(description) => description.raw.clone(),
            _ => panic!("Description not found"),
        }
        .color(Color::Cyan)
    )
    .unwrap();
    writeln!(
        output,
        "-------------------------------------------------------------"
    )
    .unwrap();
    let mut comment_colorer = DistinctColorer {
        categories: &mut HashMap::new(),
        colors: vec![
            Color::Red,
            Color::Green,
            Color::Yellow,
            Color::Blue,
            Color::Magenta,
            Color::Cyan,
            Color::White,
            Color::BrightRed,
            Color::BrightGreen,
            Color::BrightYellow,
            Color::BrightBlue,
            Color::BrightMagenta,
            Color::BrightCyan,
            Color::BrightWhite,
            Color::BrightBlack,
        ],
    };
    writeln!(output, "Comments:").unwrap();
    for comment in comments.iter().rev() {
        let author = cli.get_name_from_phid(&comment.author_phid).await.unwrap();
        writeln!(
            output,
            "==== {} - {}",
            DateTime::<Local>::from(UNIX_EPOCH + Duration::from_secs(comment.date_modified.into())),
            comment_colorer.color(
                author.clone(),
                Some(format!("{}\n{}\n", author, comment.content.raw))
            )
        )
        .unwrap();
    }
    writeln!(
        output,
        "-------------------------------------------------------------"
    )
    .unwrap();
}

fn read_stdin() -> String {
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer).unwrap();
    return buffer;
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let opts: Opts = Opts::parse();
    let config: config::TasksConf = config::TasksConf::from_file(&opts.config);
    let mut cli = client::ManiphestClient::new(&config);

    if opts.no_color {
        control::set_override(false);
    } else {
        control::set_override(true);
    }

    match opts.subcommand {
        SubCommand::List(t) => {
            if t.debug {
                println!("Running command list: query={}", t.saved_search);
            }
            match cli.get_tasks_by_saved_search(&t.saved_search).await {
                Ok(tasks) => {
                    let mut output = String::new();
                    if t.only_ids {
                        for task in tasks {
                            match task.id {
                                Some(task_id) => println!("T{}", task_id),
                                None => (),
                            };
                        }
                    } else {
                        print_tasks(&mut cli, &tasks, &mut output, false).await;
                        open_pager(&output);
                    }
                }
                Err(e) => println!("Error fetching the tasks, {}", e),
            }
        }
        SubCommand::ListSavedSearches(t) => {
            if t.debug {
                println!("Running command list searches");
            }
            print_searches(&config)
        }
        SubCommand::Show(t) => {
            if t.debug {
                println!("Running command show: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            let comments = match t.without_comments {
                true => Vec::new(),
                false => cli.get_task_comments(&task_id).await?,
            };
            match cli.get_task_details(&task_id).await {
                Ok(task) => {
                    let mut output = String::new();
                    print_task_details(&mut cli, &task, &comments, &mut output, t.with_phids).await;
                    open_pager(&output);
                }
                Err(e) => println!("Error fetching the task, {}", e),
            }
        }
        SubCommand::ShowBoard(t) => {
            if t.debug {
                println!("Running command show board");
            }
            match cli.get_board(&t.project_tag).await {
                Ok(board) => {
                    let mut output = String::new();
                    print_board(&mut cli, &board, &t.column, &mut output, t.with_phids).await;
                    open_pager(&output);
                }
                Err(e) => println!("Error fetching the task, {}", e),
            }
        }
        SubCommand::AddComment(t) => {
            if t.debug {
                println!(
                    "Running command AddComment: task_id={:?}, comment={:?}",
                    t.task_id, t.comment
                );
            };
            let task_id = parse_task(&config, t.task_id);
            let comment = match t.comment {
                Some(raw_comment) => match raw_comment.as_str() {
                    "-" => read_stdin(),
                    _ => raw_comment,
                },
                None => {
                    // Skip task coloring
                    control::set_override(false);
                    match cli.get_task_details(&task_id).await {
                        Ok(task) => {
                            let comments = cli.get_task_comments(&task_id).await?;
                            let mut output = String::new();
                            print_task_details(&mut cli, &task, &comments, &mut output, false)
                                .await;
                            let template = output
                                .split("\n")
                                .into_iter()
                                .map(|line| format!("## {}", line))
                                .fold(String::new(), |all_text, next_line| {
                                    format!("{}\n{}", all_text, next_line)
                                });
                            editor::read_from_editor(Some(&template))
                        }
                        Err(e) => panic!("Error fetching the task, {}", e),
                    }
                }
            };
            let trimmed_comments =
                comment
                    .split("\n")
                    .into_iter()
                    .fold(String::new(), |all_text, next_line| {
                        if next_line.starts_with("## ") {
                            all_text
                        } else if all_text.len() == 0 {
                            next_line.to_string()
                        } else if next_line.trim().len() == 0 && all_text.len() == 0 {
                            // all empty lines until now, skip them all
                            all_text
                        } else {
                            format!("{}\n{}", all_text, next_line)
                        }
                    });
            if trimmed_comments.len() == 0 {
                println!("Skipping comment, as an empty one was given.");
                return Ok(());
            }
            match cli.comment_on_task(&task_id, &trimmed_comments).await {
                Ok(_) => println!("Comment added to {}.", task_id),
                Err(e) => println!("Error adding a comment to the task: {}", e),
            }
        }
        SubCommand::Create(t) => {
            if t.debug {
                println!("Running command Create");
            }
            let description = match t.description {
                Some(raw_description) => match raw_description.as_str() {
                    "-" => read_stdin(),
                    _ => raw_description,
                },
                None => {
                    editor::read_from_editor(Some(&"## Write the description below".to_string()))
                }
            };
            let origin_tag_phid = cli.get_phid_from_name(&(t.origin.to_tag())).await?;
            let worktype_tag_phid = cli.get_phid_from_name(&(t.worktype.to_tag())).await?;
            match cli
                .create_task(
                    &t.title,
                    &description,
                    t.parent,
                    t.claim,
                    t.priority,
                    origin_tag_phid,
                    worktype_tag_phid,
                )
                .await
            {
                Ok(edit_result) => {
                    set_last_task(&config, &format!("T{}", edit_result).to_string());
                    println!("Task created T{}", edit_result)
                }
                Err(e) => println!("Error creating task: {}", e),
            }
        }
        SubCommand::Claim(t) => {
            if t.debug {
                println!("Running command Claim: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.claim_task(&task_id).await {
                Ok(_) => println!("Now {} it's yours ;)", task_id),
                Err(e) => println!("Error claiming to the task: {}", e),
            }
        }
        SubCommand::Subscribe(t) => {
            if t.debug {
                println!("Running command Subscribe: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.subscribe(&task_id).await {
                Ok(_) => println!("You are now subscribed to {} :}}", task_id),
                Err(e) => println!("Error subscribing to the task: {}", e),
            }
        }
        SubCommand::Untag(t) => {
            if t.debug {
                println!(
                    "Running command Untag: task_id={:?}, tag={}",
                    t.task_id, t.tag
                );
            }
            let task_id = parse_task(&config, t.task_id);
            let tag_phid = cli
                .get_phid_from_name(&client::ensure_tag(t.tag.clone()))
                .await
                .unwrap();
            match cli.untag(&task_id, &tag_phid).await {
                Ok(_) => println!("Tag {} removed from {} :}}", t.tag, task_id),
                Err(e) => println!(
                    "Error removing tag {} from to the task {}: {}",
                    t.tag, task_id, e
                ),
            }
        }
        SubCommand::Tag(t) => {
            if t.debug {
                println!(
                    "Running command Tag: task_id={:?}, tag={}",
                    t.task_id, t.tag
                );
            }
            let task_id = parse_task(&config, t.task_id);
            let tag_phid = cli
                .get_phid_from_name(&client::ensure_tag(t.tag.clone()))
                .await
                .unwrap();
            match cli.tag(&task_id, &tag_phid).await {
                Ok(_) => println!("Tag {} added to {} :}}", t.tag, task_id),
                Err(e) => println!("Error adding tag {} to the task {}: {}", t.tag, task_id, e),
            }
        }
        SubCommand::Search(t) => {
            if t.debug {
                println!("Running command Search: query={}", t.query);
            }
            match cli.search_tasks(&t.query).await {
                Ok(tasks) => {
                    let mut output = String::new();
                    if t.only_ids {
                        for task in tasks {
                            match task.id {
                                Some(task_id) => println!("T{}", task_id),
                                None => (),
                            };
                        }
                    } else {
                        print_tasks(&mut cli, &tasks, &mut output, false).await;
                        open_pager(&output);
                    }
                }
                Err(e) => println!("Error subscribing to the task: {}", e),
            }
        }
        SubCommand::SearchUsers(t) => {
            if t.debug {
                println!("Running command SearchUsers: query={}", t.query);
            }
            match cli.search_users(&t.query).await {
                Ok(users) => {
                    let mut output = String::new();
                    if t.only_ids {
                        for user in users {
                            println!(
                                "{}",
                                match user.fields.get("username").unwrap() {
                                    maniphest::UserSearchEntryField::StringField(username) =>
                                        username,
                                    _ => panic!("Username not found"),
                                }
                            );
                        }
                    } else {
                        print_users(&users, &mut output).await;
                        open_pager(&output);
                    }
                }
                Err(e) => println!("Error subscribing to the task: {}", e),
            }
        }
        SubCommand::Unsubscribe(t) => {
            if t.debug {
                println!("Running command Unsubscribe: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.unsubscribe(&task_id).await {
                Ok(_) => println!("You are now unsubscribed to {} :}}", task_id),
                Err(e) => println!("Error unsubscribing to the task: {}", e),
            }
        }
        SubCommand::Handle(t) => {
            if t.debug {
                println!("Running command handle: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            let tag_phid = cli
                .get_phid_from_name(&client::ensure_tag(cli.config.personal_project_tag.clone()))
                .await
                .unwrap();
            match cli.tag(&task_id, &tag_phid).await {
                Ok(_) => println!(
                    "Personal project tag ({}) added to {} :}}",
                    cli.config.personal_project_tag, task_id
                ),
                Err(e) => println!(
                    "Error adding personal project tag ({}) from {}: {}",
                    cli.config.personal_project_tag, task_id, e
                ),
            }
        }
        SubCommand::Unhandle(t) => {
            if t.debug {
                println!("Running command unhandle: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            let tag_phid = cli
                .get_phid_from_name(&client::ensure_tag(cli.config.personal_project_tag.clone()))
                .await
                .unwrap();
            match cli.untag(&task_id, &tag_phid).await {
                Ok(_) => println!(
                    "Personal project tag ({}) removed from {} :}}",
                    cli.config.personal_project_tag, task_id
                ),
                Err(e) => println!(
                    "Error removing personal project tag ({}) from {}: {}",
                    cli.config.personal_project_tag, task_id, e
                ),
            }
        }
        SubCommand::Close(t) => {
            if t.debug {
                println!("Running command Close: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.close_task(&task_id).await {
                Ok(_) => println!("Now {} it's closed XP", task_id),
                Err(e) => println!("Error closing to the task: {}", e),
            }
        }
        SubCommand::Done(t) => {
            if t.debug {
                println!("Running command complete: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli
                .move_to(&task_id, models::BoardColumn::from_str("ddone").unwrap())
                .await
            {
                Ok(_) => println!("Set {} as done.", task_id,),
                Err(e) => println!("Error setting {} as done: {}", task_id, e),
            }
            match cli.close_task(&task_id).await {
                Ok(_) => println!("And closed."),
                Err(e) => println!("Error closing {} when done: {}", task_id, e),
            }
        }
        SubCommand::ChangePriority(t) => {
            if t.debug {
                println!(
                    "Running command ChangePriority: task_id={:?} new_priority={}",
                    t.task_id, t.new_priority
                );
            }
            let task_id = parse_task(&config, t.task_id);
            let new_priority = models::TaskPriority::from_str(t.new_priority.as_str()).unwrap();
            match cli.change_priority(&task_id, new_priority).await {
                Ok(_) => println!("Changed priority of {} to {} B-)", task_id, t.new_priority),
                Err(e) => println!("Error changing the priority of the task: {}", e),
            }
        }
        SubCommand::SetStatus(t) => {
            if t.debug {
                println!(
                    "Running command SetStatus: task_id={:?} new_status={}",
                    t.task_id, t.new_status
                );
            }
            let task_id = parse_task(&config, t.task_id);
            let new_status = models::TaskStatus::from_str(t.new_status.as_str()).unwrap();
            match cli.set_status(&task_id, new_status).await {
                Ok(_) => println!("Changed status of {} to {} B-)", task_id, t.new_status),
                Err(e) => println!("Error changing the status of the task: {}", e),
            }
        }
        SubCommand::SetTitle(t) => {
            if t.debug {
                println!(
                    "Running command SetTitle: task_id={:?} new_title={}",
                    t.task_id, t.new_title
                );
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.change_title(&task_id, &t.new_title).await {
                Ok(_) => println!("Changed title of {} to {} B-)", task_id, t.new_title),
                Err(e) => println!("Error changing the priority of the task: {}", e),
            }
        }
        SubCommand::EditDescription(t) => {
            if t.debug {
                println!(
                    "Running command EditDescription: task_id={:?} new_title={:?}",
                    t.task_id, t.new_description
                );
            }
            let task_id = parse_task(&config, t.task_id);
            let description = match t.new_description {
                Some(new_description) => match new_description.as_str() {
                    "-" => read_stdin(),
                    _ => new_description,
                },
                None => match cli.get_task_details(&task_id).await {
                    Ok(task) => {
                        editor::read_from_editor(match task.fields.get("description").unwrap() {
                            models::Field::Description(description) => Some(&description.raw),
                            _ => panic!("Description not found"),
                        })
                    }
                    Err(e) => panic!("Error fetching the task, {}", e),
                },
            };
            match cli.change_description(&task_id, &description).await {
                Ok(_) => println!("Changed description of {} to {} B-)", task_id, description),
                Err(e) => println!("Error changing the priority of the task: {}", e),
            }
        }
        SubCommand::ForToday(t) => {
            if t.debug {
                println!("Running command fortoday: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.move_to(&task_id, models::BoardColumn::DToday).await {
                Ok(_) => println!("Moved {} to today.", task_id,),
                Err(e) => println!("Error moving {} to for today: {}", task_id, e),
            }
            match cli.set_status(&task_id, models::TaskStatus::Open).await {
                Ok(_) => println!("Set {} as open.", task_id,),
                Err(e) => println!("Error setting {} as open: {}", task_id, e),
            }
        }
        SubCommand::GetName(t) => {
            if t.debug {
                println!("Running command getname: phid={}", t.phid);
            }
            match cli.get_name_from_phid(&t.phid).await {
                Ok(name) => println!("{}", name),
                Err(e) => println!("Error retrieving name for phid:{}: {}", t.phid, e),
            }
        }
        SubCommand::GetPHID(t) => {
            if t.debug {
                println!("Running command getphid: name={}", t.name);
            }
            match cli.get_phid_from_name(&t.name).await {
                Ok(name) => println!("{}", name),
                Err(e) => match e.downcast::<maniphest::SearchError>() {
                    Ok(_) => panic!("Name not found: {}", t.name),
                    Err(e) => println!("Unknown error retrieving phid for name:{}: {}", t.name, e),
                },
            }
        }
        SubCommand::Refine(t) => {
            if t.debug {
                println!("Running command torefined: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.move_to(&task_id, models::BoardColumn::DRefined).await {
                Ok(_) => println!("Moved {} to refined.", task_id,),
                Err(e) => println!("Error moving {} to refined: {}", task_id, e),
            }
            match cli.set_status(&task_id, models::TaskStatus::Open).await {
                Ok(_) => println!("Set {} as open.", task_id,),
                Err(e) => println!("Error setting {} as open: {}", task_id, e),
            }
        }
        SubCommand::Backlog(t) => {
            if t.debug {
                println!("Running command torefine: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.move_to(&task_id, models::BoardColumn::DBacklog).await {
                Ok(_) => println!("Moved {} to refine.", task_id,),
                Err(e) => println!("Error moving {} to refine: {}", task_id, e),
            }
            match cli.set_status(&task_id, models::TaskStatus::Open).await {
                Ok(_) => println!("Set {} as open.", task_id,),
                Err(e) => println!("Error setting {} as open: {}", task_id, e),
            }
        }
        SubCommand::MoveTo(t) => {
            if t.debug {
                println!(
                    "Running command MoveTo: task_id={:?} new_column={}",
                    t.task_id, t.new_column
                );
            }
            let task_id = parse_task(&config, t.task_id);
            let new_column = models::BoardColumn::from_str(t.new_column.as_str()).unwrap();
            match cli.move_to(&task_id, new_column).await {
                Ok(_) => println!("Changed priority of {} to {} B-)", task_id, t.new_column),
                Err(e) => println!("Error changing the priority of the task: {}", e),
            }
        }
        SubCommand::Open(t) => {
            if t.debug {
                println!("Running command Open: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            let last_slash = config.host.url.rfind("/").unwrap();
            let url = format!("{}/{}", config.host.url.get(..last_slash).unwrap(), task_id);
            open_browser(&url);
        }
        SubCommand::AddParent(t) => {
            if t.debug {
                println!(
                    "Running command AddParent: child_task_id={:?} parent_task_id={:?}",
                    t.child_task_id, t.parent_task_id
                );
            }
            let child_task_id = parse_task(&config, t.child_task_id);
            match cli.add_parent(&child_task_id, &t.parent_task_id).await {
                Ok(_) => println!(
                    "Made task {} a parent of {}",
                    t.parent_task_id, child_task_id,
                ),
                Err(e) => println!(
                    "Error adding parent {} to the task {}: {}",
                    t.parent_task_id, child_task_id, e
                ),
            }
        }
        SubCommand::AddSubscriber(t) => {
            if t.debug {
                println!(
                    "Running command AddSubscriber: task_id={:?} user_name={:?}",
                    t.task_id, t.user_name
                );
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.add_subscriber(&task_id, &t.user_name).await {
                Ok(_) => println!("Subscribed {} to the task {}", t.user_name, task_id,),
                Err(e) => println!(
                    "Error subscribing {} to the task {}: {}",
                    t.user_name, task_id, e
                ),
            }
        }
        SubCommand::RemoveSubscriber(t) => {
            if t.debug {
                println!(
                    "Running command RemoveSubscriber: task_id={:?} user_name={:?}",
                    t.task_id, t.user_name
                );
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.remove_subscriber(&task_id, &t.user_name).await {
                Ok(_) => println!("Unsubscribed {} to the task {}", t.user_name, task_id,),
                Err(e) => println!(
                    "Error unsubscribing {} to the task {}: {}",
                    t.user_name, task_id, e,
                ),
            }
        }
        SubCommand::Assign(t) => {
            if t.debug {
                println!(
                    "Running command Assign: task_id={:?} user_name={:?}",
                    t.task_id, t.user_name
                );
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.assign(&task_id, &t.user_name).await {
                Ok(_) => println!("Assigned task {} to user '{}'", task_id, t.user_name,),
                Err(e) => println!(
                    "Error assigning task {} to user {}: {}",
                    task_id, t.user_name, e
                ),
            }
        }
        SubCommand::Unassign(t) => {
            if t.debug {
                println!("Running command Unassign: task_id={:?}", t.task_id,);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli.unassign(&task_id).await {
                Ok(_) => println!("Unassigned task {}", task_id,),
                Err(e) => println!("Error unassigning task {}: {}", task_id, e),
            }
        }
        SubCommand::Block(t) => {
            if t.debug {
                println!("Running command block: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli
                .move_to(&task_id, models::BoardColumn::from_str("dblocked").unwrap())
                .await
            {
                Ok(_) => println!("Moved {} to blocked.", task_id,),
                Err(e) => println!("Error moving {} to blocked: {}", task_id, e),
            }
            match cli.set_status(&task_id, models::TaskStatus::Stalled).await {
                Ok(_) => println!("Set {} as stalled.", task_id,),
                Err(e) => println!("Error setting {} as stalled: {}", task_id, e),
            }
        }
        SubCommand::Doing(t) => {
            if t.debug {
                println!("Running command doing: task_id={:?}", t.task_id);
            }
            let task_id = parse_task(&config, t.task_id);
            match cli
                .move_to(&task_id, models::BoardColumn::from_str("ddoing").unwrap())
                .await
            {
                Ok(_) => println!("Moved {} to doing.", task_id,),
                Err(e) => println!("Error moving {} to doing: {}", task_id, e),
            }
            match cli
                .set_status(&task_id, models::TaskStatus::InProgress)
                .await
            {
                Ok(_) => println!("Set {} as in progress.", task_id,),
                Err(e) => println!("Error setting {} as in progress: {}", task_id, e),
            }
            match cli.claim_task(&task_id).await {
                Ok(_) => println!("Claimed {}.", task_id,),
                Err(e) => println!("Error claiming {}: {}", task_id, e),
            }
        }
        SubCommand::RemoveParent(t) => {
            if t.debug {
                println!(
                    "Running command RemoveParent: child_task_id={:?} parent_task_id={:?}",
                    t.child_task_id, t.parent_task_id
                );
            }
            let child_task_id = parse_task(&config, t.child_task_id);
            match cli.remove_parent(&child_task_id, &t.parent_task_id).await {
                Ok(_) => println!(
                    "Removed task {} as parent of {}",
                    t.parent_task_id, child_task_id,
                ),
                Err(e) => println!(
                    "Error removing parent {} to the task {}: {}",
                    t.parent_task_id, child_task_id, e
                ),
            }
        }
        SubCommand::Phaste(t) => {
            if t.debug {
                println!("Running command Phaste: text={:?}", t.text);
            }
            let text = match t.text {
                Some(data) => data,
                None => read_stdin(),
            };
            match cli.phaste(&text).await {
                Ok(res) => println!("Created paste {}", res),
                Err(e) => println!("Error creating phaste with text({}): {}", text, e),
            }
        }
    }
    Ok(())
}
