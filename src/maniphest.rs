use clap::ValueEnum;
use reqwest::Client;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;
use std::fmt;

use super::config;
use super::models;

#[derive(Debug)]
pub struct SearchError {
    pub message: String,
}

impl fmt::Display for SearchError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "An Error Occurred: {}", self.message)
    }
}

impl Error for SearchError {}

#[derive(ValueEnum, Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum TaskOrigin {
    User,
    Team,
    Alert,
}

impl TaskOrigin {
    pub fn to_tag(self) -> String {
        match self {
            TaskOrigin::User => "#wmcs-o-user".to_string(),
            TaskOrigin::Team => "#wmcs-o-team".to_string(),
            TaskOrigin::Alert => "#wmcs-o-alert".to_string(),
        }
    }
}

#[derive(ValueEnum, Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum TaskWorktype {
    Project,
    Unplanned,
    Maintenance,
}

impl TaskWorktype {
    pub fn to_tag(self) -> String {
        match self {
            TaskWorktype::Project => "#wmcs-w-project".to_string(),
            TaskWorktype::Unplanned => "#wmcs-w-unplanned".to_string(),
            TaskWorktype::Maintenance => "#wmcs-w-maintenance".to_string(),
        }
    }
}

// a maniphest edit transaction (to add comments, change owner...)
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub struct TransactionRequest {
    #[serde(rename = "type")]
    pub transaction_type: String,
    pub value: TransactionRequestValue,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum TransactionRequestValue {
    String(String),
    Vector(Vec<String>),
    Null,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct SearchResponse {
    pub result: SearchResult,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct SearchResult {
    pub data: Vec<models::Task>,
    pub maps: HashMap<String, String>,
    pub query: HashMap<String, Option<String>>,
    pub cursor: Cursor,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UserSearchResponse {
    pub result: UserSearchResult,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct UserSearchResult {
    pub data: Vec<UserSearchEntry>,
    pub maps: HashMap<String, String>,
    pub query: HashMap<String, Option<String>>,
    pub cursor: Cursor,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UserSearchEntry {
    pub id: u16,
    pub phid: String,
    pub fields: HashMap<String, UserSearchEntryField>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(untagged, rename_all = "camelCase")]
pub enum UserSearchEntryField {
    StringField(String),
    ListField(Vec<String>),
    None,
    OtherValue(serde_json::Value),
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct EdgeSearchResponse {
    pub result: EdgeSearchResult,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct EdgeSearchResult {
    pub data: Vec<models::Edge>,
    pub cursor: Cursor,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct TransactionSearchResponse {
    pub result: TransactionSearchResult,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct TransactionSearchResult {
    pub data: Vec<Transaction>,
    pub cursor: Cursor,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Transaction {
    pub id: u32,
    pub phid: String,
    #[serde(rename = "type")]
    pub transaction_type: Option<String>,
    #[serde(rename = "authorPHID")]
    pub author_phid: String,
    #[serde(rename = "objectPHID")]
    pub object_phid: String,
    pub date_created: u32,
    pub date_modified: u32,
    #[serde(rename = "groupID")]
    pub group_id: Option<String>,
    pub comments: Vec<Comment>,
    pub fields: HashMap<String, models::Field>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Comment {
    pub id: u32,
    pub phid: String,
    pub version: u16,
    #[serde(rename = "authorPHID")]
    pub author_phid: String,
    pub date_created: u32,
    pub date_modified: u32,
    pub removed: bool,
    pub content: Content,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct Content {
    pub raw: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct Cursor {
    pub limit: i32,
    pub after: Option<String>,
    pub before: Option<String>,
    pub order: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct PHIDLookupResponse {
    pub result: PHIDLookupResult,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(untagged)]
pub enum PHIDLookupResult {
    NoResult(Vec<String>),
    Result(HashMap<String, PHIDLookupNonEmptyResult>),
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PHIDLookupNonEmptyResult {
    pub phid: String,
    pub uri: String,
    pub type_name: String,
    #[serde(rename = "type")]
    pub type_code: String,
    pub name: String,
    pub full_name: String,
    pub status: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ProjectColumnSearchResponse {
    pub result: ProjectColumnSearchResult,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct ProjectColumnSearchResult {
    pub data: Vec<models::ProjectColumn>,
    pub maps: HashMap<String, String>,
    pub query: HashMap<String, Option<String>>,
    pub cursor: Cursor,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UserQueryResponse {
    pub result: Option<Vec<models::User>>,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ProjectQueryResponse {
    pub result: Option<ProjectQueryResult>,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProjectQueryResult {
    pub data: HashMap<String, models::Project>,
    pub cursor: Cursor,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct EditResponse {
    pub result: EditResponseResult,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct EditResponseResult {
    pub object: PhabObject,
    pub transactions: Vec<PhabTransactionID>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AssignResult {
    pub object: PhabObject,
    pub transactions: Vec<PhabTransactionID>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PhabObject {
    pub id: i32,
    pub phid: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PhabTransactionID {
    pub phid: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct WhoamiResponse {
    pub result: models::User,
    pub error_code: Option<String>,
    pub error_info: Option<String>,
}

fn transaction_to_params(transaction: &TransactionRequest, index: usize) -> Vec<(String, String)> {
    let mut params = vec![(
        format!("transactions[{}][type]", index),
        transaction.transaction_type.clone(),
    )];
    match &transaction.value {
        TransactionRequestValue::String(value) => {
            params.push((format!("transactions[{}][value]", index), value.clone()))
        }
        TransactionRequestValue::Vector(strings) => {
            params.extend(strings.iter().enumerate().map(|index_val| {
                (
                    format!("transactions[{}][value][{}]", index, index_val.0),
                    index_val.1.clone(),
                )
            }))
        }
        TransactionRequestValue::Null => {
            params.push((format!("transactions[{}][value]", index), String::from("")))
        }
    };
    params
}

pub async fn search_tasks_page(
    conf: &config::TasksConf,
    project_name: Option<String>,
    query: Option<String>,
    after: Option<i32>,
) -> models::MaybeResult<(Vec<models::Task>, i32)> {
    let mut params = vec![
        ("api.token".to_owned(), conf.host.token.clone()),
        ("queryKey".to_owned(), "".to_string()),
        // Note that 'open' is a status, 'open()' is any open-like status
        ("constraints[statuses][0]".to_owned(), "open()".to_string()),
        ("attachments[columns]".to_owned(), "true".to_string()),
        ("attachments[projects]".to_owned(), "true".to_string()),
    ];
    match project_name {
        Some(project_name) => params.push((
            "constraints[projects][0]".to_owned(),
            project_name.to_owned(),
        )),
        None => (),
    }
    match query {
        Some(query) => params.push(("constraints[query]".to_owned(), query.to_owned())),
        None => params.push(("constraints[query]".to_owned(), "".to_string())),
    }
    match after {
        Some(after_num) => {
            params.push(("after".to_owned(), after_num.to_string()));
        }
        None => (),
    };
    let res = Client::new()
        .get(&format!("{}/maniphest.search", conf.host.url))
        .query(&params)
        .send()
        .await?;
    let raw_response: String = res.text().await?;
    let response: SearchResponse = serde_json::from_str(raw_response.as_str()).expect(
        format!(
            "Failed to deserialize response, raw response: {}",
            raw_response
        )
        .as_str(),
    );
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => Ok((
            response.result.data,
            response
                .result
                .cursor
                .after
                .unwrap_or("0".to_string())
                .parse::<i32>()
                .unwrap_or(0),
        )),
    }
}

pub async fn search_tasks_paged(
    conf: &config::TasksConf,
    project_name: Option<String>,
    query: Option<String>,
) -> models::MaybeResult<Vec<models::Task>> {
    let (mut tasks, mut after) = search_tasks_page(conf, project_name.clone(), query.clone(), None)
        .await
        .unwrap();
    while after > 0 {
        let result = search_tasks_page(conf, project_name.clone(), query.clone(), Some(after))
            .await
            .unwrap();
        tasks.extend(result.0);
        after = result.1;
    }
    println!("# Got {} tasks.", tasks.len());
    Ok(tasks)
}

pub async fn get_tasks_for_project(
    conf: &config::TasksConf,
    project_name: &String,
) -> models::MaybeResult<Vec<models::Task>> {
    search_tasks_paged(conf, Some(project_name.to_owned()), None).await
}

pub async fn search_users(
    conf: &config::TasksConf,
    query: &String,
) -> models::MaybeResult<Vec<UserSearchEntry>> {
    let params = [
        ("api.token", conf.host.token.as_str()),
        ("constraints[query]", query),
    ];
    let res = Client::new()
        .get(&format!("{}/user.search", conf.host.url))
        .query(&params)
        .send()
        .await?;
    let raw_response: String = res.text().await?;
    let response: UserSearchResponse = serde_json::from_str(raw_response.as_str()).expect(
        format!(
            "Failed to deserialize response, raw response: {}",
            raw_response
        )
        .as_str(),
    );
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => Ok(response.result.data),
    }
}

pub async fn search_tasks(
    conf: &config::TasksConf,
    query: &String,
) -> models::MaybeResult<Vec<models::Task>> {
    search_tasks_paged(conf, None, Some(query.to_owned())).await
}

pub async fn get_tasks_by_saved_search(
    conf: &config::TasksConf,
    saved_search: &String,
) -> models::MaybeResult<Vec<models::Task>> {
    let query_key = conf.searches.get(saved_search).unwrap_or(saved_search);
    let params = [
        ("api.token", conf.host.token.as_str()),
        ("queryKey", query_key),
        ("attachments[columns]", "true"),
        ("attachments[projects]", "true"),
    ];
    let res = Client::new()
        .get(&format!("{}/maniphest.search", conf.host.url))
        .query(&params)
        .send()
        .await?;
    let raw_response: String = res.text().await?;
    let response: SearchResponse = serde_json::from_str(raw_response.as_str()).expect(
        format!(
            "Failed to deserialize response, raw response: {}",
            raw_response
        )
        .as_str(),
    );
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => Ok(response.result.data),
    }
}

pub async fn get_user(
    conf: &config::TasksConf,
    phid: &String,
) -> models::MaybeResult<models::User> {
    let params = [("api.token", conf.host.token.as_str()), ("phids[0]", phid)];
    let raw_response: String = Client::new()
        .get(&format!("{}/user.query", conf.host.url))
        .query(&params)
        .send()
        .await?
        .text()
        .await?;
    let response: UserQueryResponse = serde_json::from_str(raw_response.as_str())
        .expect(format!("Unsupported response {}", raw_response).as_str());
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => Ok(models::User::from(
            response.result.unwrap().get(0).unwrap().to_owned(),
        )),
    }
}

async fn apply_transactions(
    conf: &config::TasksConf,
    transactions: &Vec<TransactionRequest>,
    object_identifier: Option<&String>,
    method: Option<&str>,
) -> models::MaybeResult<EditResponseResult> {
    let mut params = vec![("api.token".to_string(), conf.host.token.clone())];
    match object_identifier {
        Some(object_phid) => params.push(("objectIdentifier".to_string(), object_phid.to_owned())),
        None => (),
    };
    params.extend(
        transactions
            .iter()
            .enumerate()
            .map(|index_tran| transaction_to_params(index_tran.1, index_tran.0))
            .flatten(),
    );
    let url = match method {
        Some(method_name) => format!("{}/{}", conf.host.url, method_name),
        None => format!("{}/maniphest.edit", conf.host.url),
    };
    let raw_response: String = Client::new()
        .get(&url)
        .query(&params)
        .send()
        .await?
        .text()
        .await?;
    let response: EditResponse = serde_json::from_str(raw_response.as_str())
        .expect(format!("Unsupported response {}", raw_response).as_str());
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => Ok(response.result),
    }
}

pub async fn get_task_comments(
    conf: &config::TasksConf,
    task_id: &String,
) -> models::MaybeResult<Vec<Comment>> {
    let params = [
        ("api.token", conf.host.token.as_str()),
        ("objectIdentifier", task_id),
    ];
    let res = Client::new()
        .get(&format!("{}/transaction.search", conf.host.url))
        .query(&params)
        .send()
        .await?;
    let raw_response: String = res.text().await?;
    let response: TransactionSearchResponse = serde_json::from_str(raw_response.as_str()).expect(
        format!(
            "Failed to deserialize response, raw response: {}",
            raw_response
        )
        .as_str(),
    );
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => Ok(response
            .result
            .data
            .iter()
            .filter(|transaction| {
                transaction
                    .transaction_type
                    .clone()
                    .unwrap_or("".to_string())
                    == "comment"
            })
            .map(|transaction| {
                transaction
                    .comments
                    .iter()
                    .filter(|comment| !comment.removed)
                    .map(|comment| comment.clone())
                    .collect::<Vec<Comment>>()
            })
            .flatten()
            .collect()),
    }
}

pub async fn get_task_details_by_phid(
    conf: &config::TasksConf,
    task_phid: &String,
) -> models::MaybeResult<models::Task> {
    let params = [
        ("api.token", conf.host.token.as_str()),
        ("constraints[phids][0]", task_phid),
        ("attachments[columns]", "true"),
        ("attachments[subscribers]", "true"),
        ("attachments[projects]", "true"),
    ];
    let res = Client::new()
        .get(&format!("{}/maniphest.search", conf.host.url))
        .query(&params)
        .send()
        .await?;
    let raw_response: String = res.text().await?;
    let response: SearchResponse = serde_json::from_str(raw_response.as_str()).expect(
        format!(
            "Failed to deserialize response, raw response: {}",
            raw_response
        )
        .as_str(),
    );
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => match response.result.data.get(0) {
            Some(task) => Ok(task.clone()),
            None => Err(Box::new(SearchError {
                message: format!("Task with phid {} not found", task_phid),
            })),
        },
    }
}

pub async fn get_task_details_by_id(
    conf: &config::TasksConf,
    task_id: &String,
) -> models::MaybeResult<models::Task> {
    let task_id_str = {
        if task_id.starts_with('T') {
            &task_id[1..]
        } else {
            task_id.as_str()
        }
    };
    let params = [
        ("api.token", conf.host.token.as_str()),
        ("constraints[ids][0]", task_id_str),
        ("attachments[columns]", "true"),
        ("attachments[subscribers]", "true"),
        ("attachments[projects]", "true"),
    ];
    let res = Client::new()
        .get(&format!("{}/maniphest.search", conf.host.url))
        .query(&params)
        .send()
        .await?;
    let raw_response: String = res.text().await?;
    let response: SearchResponse = serde_json::from_str(raw_response.as_str()).expect(
        format!(
            "Failed to deserialize response, raw response: {}",
            raw_response
        )
        .as_str(),
    );
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => match response.result.data.get(0) {
            Some(task) => Ok(task.clone()),
            None => Err(Box::new(SearchError {
                message: format!("Task with id {} not found", task_id_str),
            })),
        },
    }
}

pub async fn create_task(
    conf: &config::TasksConf,
    title: &String,
    description: &String,
    maybe_parent: Option<String>,
    claim: bool,
    maybe_priority: Option<String>,
    origin: String,
    worktype: String,
) -> models::MaybeResult<EditResponseResult> {
    let mut transactions = vec![
        TransactionRequest {
            transaction_type: "title".to_string(),
            value: TransactionRequestValue::String(title.to_owned()),
        },
        TransactionRequest {
            transaction_type: "description".to_string(),
            value: TransactionRequestValue::String(description.to_owned()),
        },
        TransactionRequest {
            transaction_type: "projects.add".to_string(),
            value: TransactionRequestValue::Vector(vec![
                conf.personal_project_tag.clone(),
                conf.team_project_tag.clone(),
                origin,
                worktype,
            ]),
        },
    ];
    match maybe_parent {
        Some(parent_id) => transactions.push(TransactionRequest {
            transaction_type: "parents.add".to_string(),
            value: TransactionRequestValue::Vector(vec![get_task_details_by_id(&conf, &parent_id)
                .await
                .unwrap()
                .phid
                .unwrap()]),
        }),
        None => (),
    }
    if claim {
        let me = whoami(conf).await?;
        transactions.push(TransactionRequest {
            transaction_type: "owner".to_string(),
            value: TransactionRequestValue::String(me.phid),
        });
    }
    match maybe_priority {
        Some(priority) => transactions.push(TransactionRequest {
            transaction_type: "priority".to_string(),
            value: TransactionRequestValue::String(priority.to_string()),
        }),
        None => (),
    }
    apply_transactions(&conf, &transactions, None, None).await
}

pub async fn comment_on_task(
    conf: &config::TasksConf,
    task_id: &String,
    comment: &String,
) -> models::MaybeResult<EditResponseResult> {
    let transaction = TransactionRequest {
        transaction_type: "comment".to_string(),
        value: TransactionRequestValue::String(comment.to_owned()),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn whois(conf: &config::TasksConf, user_name: &String) -> models::MaybeResult<String> {
    return match &search_users(conf, user_name).await.unwrap()[..] {
        [user_obj] => Ok(user_obj.phid.clone()),
        response => Err(Box::new(SearchError {
            message: format!(
                "Error trying to query users for '{}', got {:?} ",
                user_name, response
            ),
        })),
    };
}

pub async fn whoami(conf: &config::TasksConf) -> models::MaybeResult<models::User> {
    let params = [("api.token", conf.host.token.as_str())];
    let raw_response: String = Client::new()
        .get(&format!("{}/user.whoami", conf.host.url))
        .query(&params)
        .send()
        .await?
        .text()
        .await?;
    let response: WhoamiResponse = serde_json::from_str(raw_response.as_str())
        .expect(format!("Unsupported response {}", raw_response).as_str());
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => Ok(response.result),
    }
}

pub async fn claim_task(
    conf: &config::TasksConf,
    task_id: &String,
) -> models::MaybeResult<EditResponseResult> {
    let me = whoami(conf).await?;
    let transaction = TransactionRequest {
        transaction_type: "owner".to_string(),
        value: TransactionRequestValue::String(me.phid),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn subscribe(
    conf: &config::TasksConf,
    task_id: &String,
) -> models::MaybeResult<EditResponseResult> {
    let me = whoami(conf).await?;
    let transaction = TransactionRequest {
        transaction_type: "subscribers.add".to_string(),
        value: TransactionRequestValue::Vector(vec![me.phid]),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn tag(
    conf: &config::TasksConf,
    task_id: &String,
    tag_phid: &String,
) -> models::MaybeResult<EditResponseResult> {
    let transaction = TransactionRequest {
        transaction_type: "projects.add".to_string(),
        value: TransactionRequestValue::Vector(vec![tag_phid.to_owned()]),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn untag(
    conf: &config::TasksConf,
    task_id: &String,
    tag: &String,
) -> models::MaybeResult<EditResponseResult> {
    let tag_phid = phid_lookup(conf, tag).await.unwrap();
    let transaction = TransactionRequest {
        transaction_type: "projects.remove".to_string(),
        value: TransactionRequestValue::Vector(vec![tag_phid]),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn unsubscribe(
    conf: &config::TasksConf,
    task_id: &String,
) -> models::MaybeResult<EditResponseResult> {
    let me = whoami(conf).await?;
    let transaction = TransactionRequest {
        transaction_type: "subscribers.remove".to_string(),
        value: TransactionRequestValue::Vector(vec![me.phid]),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn add_subscriber(
    conf: &config::TasksConf,
    task_id: &String,
    user_name: &String,
) -> models::MaybeResult<EditResponseResult> {
    let user_phid = whois(conf, user_name).await?;
    let transaction = TransactionRequest {
        transaction_type: "subscribers.add".to_string(),
        value: TransactionRequestValue::Vector(vec![user_phid]),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn remove_subscriber(
    conf: &config::TasksConf,
    task_id: &String,
    user_name: &String,
) -> models::MaybeResult<EditResponseResult> {
    let user_phid = whois(conf, user_name).await?;
    let transaction = TransactionRequest {
        transaction_type: "subscribers.remove".to_string(),
        value: TransactionRequestValue::Vector(vec![user_phid]),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn assign(
    conf: &config::TasksConf,
    task_id: &String,
    user_name: &String,
) -> models::MaybeResult<EditResponseResult> {
    let user_phid = whois(conf, user_name).await?;
    let transaction = TransactionRequest {
        transaction_type: "owner".to_string(),
        value: TransactionRequestValue::String(user_phid),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn unassign(
    conf: &config::TasksConf,
    task_id: &String,
) -> models::MaybeResult<EditResponseResult> {
    let transaction = TransactionRequest {
        transaction_type: "owner".to_string(),
        value: TransactionRequestValue::Null,
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn change_priority(
    conf: &config::TasksConf,
    task_id: &String,
    new_priority: models::TaskPriority,
) -> models::MaybeResult<EditResponseResult> {
    let transaction = TransactionRequest {
        transaction_type: "priority".to_string(),
        value: TransactionRequestValue::String(new_priority.to_string()),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn set_status(
    conf: &config::TasksConf,
    task_id: &String,
    new_status: models::TaskStatus,
) -> models::MaybeResult<EditResponseResult> {
    let transaction = TransactionRequest {
        transaction_type: "status".to_string(),
        value: TransactionRequestValue::String(new_status.to_string()),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn change_title(
    conf: &config::TasksConf,
    task_id: &String,
    new_title: &String,
) -> models::MaybeResult<EditResponseResult> {
    let transaction = TransactionRequest {
        transaction_type: "title".to_string(),
        value: TransactionRequestValue::String(new_title.to_owned()),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn change_description(
    conf: &config::TasksConf,
    task_id: &String,
    new_description: &String,
) -> models::MaybeResult<EditResponseResult> {
    let transaction = TransactionRequest {
        transaction_type: "description".to_string(),
        value: TransactionRequestValue::String(new_description.to_owned()),
    };
    apply_transactions(&conf, &vec![transaction], Some(task_id), None).await
}

pub async fn move_to(
    conf: &config::TasksConf,
    task_id: &String,
    new_column: models::BoardColumn,
) -> models::MaybeResult<EditResponseResult> {
    let transaction = TransactionRequest {
        transaction_type: "column".to_string(),
        value: {
            if new_column.to_string().starts_with('d') {
                TransactionRequestValue::Vector(vec![conf
                    .personal_project_columns
                    .get(&new_column.to_string())
                    .unwrap()
                    .to_owned()])
            } else {
                TransactionRequestValue::Vector(vec![conf
                    .team_project_columns
                    .get(&new_column.to_string())
                    .unwrap()
                    .to_owned()])
            }
        },
    };
    apply_transactions(conf, &vec![transaction], Some(task_id), None).await
}

pub async fn add_parent(
    conf: &config::TasksConf,
    child_task_id: &String,
    parent_task_id: &String,
) -> models::MaybeResult<EditResponseResult> {
    let parent_task_phid = get_task_details_by_id(&conf, parent_task_id)
        .await
        .unwrap()
        .phid
        .unwrap();
    let transaction = TransactionRequest {
        transaction_type: "parents.add".to_string(),
        value: TransactionRequestValue::Vector(vec![parent_task_phid]),
    };
    apply_transactions(conf, &vec![transaction], Some(child_task_id), None).await
}

pub async fn remove_parent(
    conf: &config::TasksConf,
    child_task_id: &String,
    parent_task_id: &String,
) -> models::MaybeResult<EditResponseResult> {
    let parent_task_phid = get_task_details_by_id(&conf, parent_task_id)
        .await
        .unwrap()
        .phid
        .unwrap();
    let transaction = TransactionRequest {
        transaction_type: "parents.remove".to_string(),
        value: TransactionRequestValue::Vector(vec![parent_task_phid]),
    };
    apply_transactions(conf, &vec![transaction], Some(child_task_id), None).await
}

pub async fn get_parent_and_children(
    conf: &config::TasksConf,
    task_id: &String,
) -> models::MaybeResult<Vec<models::Edge>> {
    let task_phid = get_task_details_by_id(&conf, task_id)
        .await
        .unwrap()
        .phid
        .unwrap();
    let params = [
        ("api.token", conf.host.token.as_str()),
        ("sourcePHIDs[0]", task_phid.as_str()),
        ("types[0]", "task.subtask"),
        ("types[1]", "task.parent"),
    ];
    let res = Client::new()
        .get(&format!("{}/edge.search", conf.host.url))
        .query(&params)
        .send()
        .await
        .unwrap();
    let raw_response: String = res.text().await.unwrap();
    let response: EdgeSearchResponse = serde_json::from_str(raw_response.as_str()).expect(
        format!(
            "Failed to deserialize response, raw response: {}",
            raw_response
        )
        .as_str(),
    );
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => Ok(response.result.data),
    }
}

pub async fn phid_lookup(conf: &config::TasksConf, name: &String) -> models::MaybeResult<String> {
    let params = [
        ("api.token", conf.host.token.as_str()),
        ("names[0]", name.as_str()),
    ];
    let res = Client::new()
        .get(&format!("{}/phid.lookup", conf.host.url))
        .query(&params)
        .send()
        .await
        .unwrap();
    let raw_response: String = res.text().await.unwrap();
    let response: PHIDLookupResponse = serde_json::from_str(raw_response.as_str()).expect(
        format!(
            "Failed to deserialize response, raw response: {}",
            raw_response
        )
        .as_str(),
    );
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => match response.result {
            PHIDLookupResult::Result(result) => Ok(result.get(name).unwrap().phid.clone()),
            _ => Err(Box::new(SearchError {
                message: format!("Got no results for name {}", name),
            })),
        },
    }
}

pub async fn name_lookup(conf: &config::TasksConf, phid: &String) -> models::MaybeResult<String> {
    let params = [
        ("api.token", conf.host.token.as_str()),
        ("phids[0]", phid.as_str()),
    ];
    let res = Client::new()
        .get(&format!("{}/phid.query", conf.host.url))
        .query(&params)
        .send()
        .await
        .unwrap();
    let raw_response: String = res.text().await.unwrap();
    let response: PHIDLookupResponse = serde_json::from_str(raw_response.as_str()).expect(
        format!(
            "Failed to deserialize response, raw response: {}",
            raw_response
        )
        .as_str(),
    );
    match response.error_info {
        Some(_) => Err(Box::new(SearchError {
            message: format!(
                "Error trying to parse response:\nRaw:{}",
                raw_response.as_str()
            ),
        })),
        None => match response.result {
            PHIDLookupResult::Result(result) => Ok(result.get(phid).unwrap().name.clone()),
            _ => Err(Box::new(SearchError {
                message: format!("Got no results for phid {}", phid),
            })),
        },
    }
}

pub async fn phaste(conf: &config::TasksConf, text: &String) -> models::MaybeResult<String> {
    let transaction = TransactionRequest {
        transaction_type: "text".to_string(),
        value: TransactionRequestValue::String(text.to_owned()),
    };
    let phid = apply_transactions(conf, &vec![transaction], None, Some("paste.edit"))
        .await
        .unwrap()
        .object
        .phid;

    name_lookup(conf, &phid).await
}
