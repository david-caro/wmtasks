use std::fs::File;
use std::io::{Read, Write};
use tempfile::tempdir;

pub fn read_from_editor(base_content: Option<&String>) -> String {
    let temp_dir = tempdir().unwrap();
    let edit_path = temp_dir.path().join("edit.wmtask");
    match base_content {
        Some(content) => {
            let display = edit_path.display();
            let mut edit_file = match File::create(&edit_path) {
                Err(why) => panic!("couldn't create {}: {}", display, why),
                Ok(file) => file,
            };

            match edit_file.write_all(content.as_bytes()) {
                Err(why) => panic!("couldn't write to {}: {}", display, why),
                Ok(_) => println!("successfully wrote to {}", display),
            }
        }
        None => (),
    }
    let mut editor = std::process::Command::new("vim")
        .arg(edit_path.clone())
        .spawn()
        .expect("command failed");

    editor.wait().expect("wait failed");
    let mut edit_file = File::open(edit_path).unwrap();
    let mut edit = String::new();
    edit_file.read_to_string(&mut edit).unwrap();
    edit
}
