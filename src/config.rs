use anyhow::Context;
use serde::Deserialize;
use serde_json::from_str;
use shellexpand::tilde;
use std::collections::HashMap;
use std::path::Path;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct TasksConf {
    pub host: HostConf,
    pub cache_path: String,
    pub personal_project_tag: String,
    pub team_project_tag: String,
    pub searches: HashMap<String, String>,
    pub personal_project_columns: HashMap<String, String>,
    pub team_project_columns: HashMap<String, String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub struct HostConf {
    pub token: String,
    pub url: String,
}

impl TasksConf {
    pub fn from_file(conf_path: &String) -> TasksConf {
        let expanded_path: String = tilde(&conf_path).into_owned();
        let path = Path::new(&expanded_path);
        if path.exists() {
            std::fs::create_dir_all(path.parent().unwrap()).unwrap_or(());
            let raw_config = std::fs::read_to_string(expanded_path.clone())
                .with_context(|| format!("Could not read file `{}`", expanded_path))
                .unwrap();
            from_str(&raw_config.to_string())
                .expect(format!("Config JSON malformed `{}`.", conf_path).as_str())
        } else {
            panic!("No config file found at {}", conf_path);
        }
    }
}
