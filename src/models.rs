use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;
use std::fmt;

pub type MaybeResult<T> = std::result::Result<T, Box<dyn Error>>;

#[derive(Debug)]
pub struct InvalidChoice {
    pub message: String,
}

impl fmt::Display for InvalidChoice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "An Error Occurred: {}", self.message)
    }
}

impl Error for InvalidChoice {}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Description {
    pub raw: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Status {
    pub value: String,
    pub name: String,
    pub color: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Priority {
    pub value: i16,
    pub name: String,
    pub color: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(untagged, rename_all = "camelCase")]
pub enum Field {
    Description(Description),
    Status(Status),
    Priority(Priority),
    DateModified(u64),
    DateCreated(u64),
    String(String),
    None,
    Value(serde_json::Value),
}

#[derive(Clone, Debug, Deserialize)]
#[serde(untagged, rename_all = "camelCase")]
pub enum Boards {
    Boards(HashMap<String, AttachedBoard>),
    Value(serde_json::Value),
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Attachment {
    pub columns: Option<AttachedColumns>,
    pub subscribers: Option<Subscribers>,
    pub projects: Option<Projects>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Projects {
    #[serde(rename = "projectPHIDs")]
    pub project_phids: Vec<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Subscribers {
    #[serde(rename = "subscriberPHIDs")]
    pub subscriber_phids: Vec<String>,
    pub subscriber_count: u32,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AttachedColumns {
    pub boards: Boards,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AttachedBoard {
    pub columns: Vec<AttachedColumn>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AttachedColumn {
    pub id: i32,
    pub phid: String,
    pub name: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Policy {
    pub view: String,
    pub interact: String,
    pub edits: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Task {
    pub id: Option<i32>,
    pub phid: Option<String>,
    pub fields: HashMap<String, Field>,
    #[serde(rename = "type")]
    pub type_name: String,
    pub attachments: Attachment,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub phid: String,
    pub user_name: String,
    pub real_name: String,
    pub image: String,
    pub uri: String,
    pub roles: Vec<String>,
}

impl ToOwned for User {
    type Owned = User;
    fn to_owned(&self) -> User {
        User {
            phid: self.phid.to_owned(),
            user_name: self.user_name.to_owned(),
            real_name: self.real_name.to_owned(),
            image: self.image.to_owned(),
            uri: self.uri.to_owned(),
            roles: self.roles.to_owned(),
        }
    }
}

impl User {
    pub fn to_string(self) -> String {
        format!("{}<{}>", self.real_name, self.user_name)
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Project {
    pub id: String,
    pub phid: String,
    pub name: String,
    #[serde(rename = "profileImagePHID")]
    pub profile_image_phid: String,
    pub icon: String,
    pub color: String,
    pub members: Vec<String>,
    pub slugs: Vec<String>,
    pub date_created: String,
    pub date_modified: String,
}

impl ToOwned for Project {
    type Owned = Project;
    fn to_owned(&self) -> Project {
        Project {
            id: self.id.to_owned(),
            phid: self.phid.to_owned(),
            name: self.name.to_owned(),
            profile_image_phid: self.profile_image_phid.to_owned(),
            icon: self.icon.to_owned(),
            color: self.color.to_owned(),
            members: self.members.iter().map(|x| x.to_owned()).collect(),
            slugs: self.slugs.iter().map(|x| x.to_owned()).collect(),
            date_created: self.date_created.to_owned(),
            date_modified: self.date_modified.to_owned(),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ProjectColumn {
    pub id: i32,
    #[serde(rename = "type")]
    pub type_name: String,
    pub phid: String,
    pub fields: ProjectColumnFields,
}

impl ToOwned for ProjectColumn {
    type Owned = ProjectColumn;
    fn to_owned(&self) -> ProjectColumn {
        ProjectColumn {
            id: self.id,
            type_name: self.type_name.to_owned(),
            phid: self.phid.to_owned(),
            fields: self.fields.to_owned(),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ProjectColumnFields {
    pub name: String,
    #[serde(rename = "proxyPHID")]
    pub proxy_phid: Option<String>,
    pub project: serde_json::Value,
    pub date_created: i32,
    pub date_modified: i32,
    pub policy: serde_json::Value,
}

impl ToOwned for ProjectColumnFields {
    type Owned = ProjectColumnFields;
    fn to_owned(&self) -> ProjectColumnFields {
        ProjectColumnFields {
            name: self.name.to_owned(),
            proxy_phid: self.proxy_phid.to_owned(),
            project: self.project.clone(),
            date_created: self.date_created,
            date_modified: self.date_modified,
            policy: self.project.clone(),
        }
    }
}

#[derive(Clone, Debug)]
pub enum TaskPriority {
    UBN,
    NeedsTriage,
    High,
    Medium,
    Low,
    Lowest,
}

impl std::str::FromStr for TaskPriority {
    type Err = Box<dyn Error>;
    fn from_str(mystr: &str) -> Result<Self, Self::Err> {
        match mystr {
            "ubn" | "u" => Some(TaskPriority::UBN),
            "needs_triage" | "nt" => Some(TaskPriority::NeedsTriage),
            "high" | "h" => Some(TaskPriority::High),
            "medium" | "m" => Some(TaskPriority::Medium),
            "low" | "l" => Some(TaskPriority::Low),
            "lowest" | "lt" => Some(TaskPriority::Lowest),
            _ => None,
        }
        .ok_or(Box::new(InvalidChoice {
            message: format!("Invalid priority {}", mystr),
        }))
    }
}

impl TaskPriority {
    pub fn to_string(self) -> String {
        match self {
            TaskPriority::UBN => "unbreak",
            TaskPriority::NeedsTriage => "triage",
            TaskPriority::High => "high",
            TaskPriority::Medium => "normal",
            TaskPriority::Low => "low",
            TaskPriority::Lowest => "lowest",
        }
        .to_string()
    }
}

#[derive(Clone, Debug)]
pub enum TaskStatus {
    Open,
    Resolved,
    InProgress,
    Stalled,
    Invalid,
    Declined,
}

impl std::str::FromStr for TaskStatus {
    type Err = Box<dyn Error>;
    fn from_str(mystr: &str) -> Result<Self, Self::Err> {
        match mystr {
            "open" | "o" => Some(TaskStatus::Open),
            "resolved" | "r" | "done" => Some(TaskStatus::Resolved),
            "in_progress" | "ip" | "doing" => Some(TaskStatus::InProgress),
            "stalled" | "s" => Some(TaskStatus::Stalled),
            "invalid" => Some(TaskStatus::Invalid),
            "declined" => Some(TaskStatus::Declined),
            _ => None,
        }
        .ok_or(Box::new(InvalidChoice {
            message: format!("Invalid status {}", mystr),
        }))
    }
}

impl TaskStatus {
    pub fn to_string(self) -> String {
        match self {
            TaskStatus::Open => "open",
            TaskStatus::Resolved => "resolved",
            TaskStatus::InProgress => "progress",
            TaskStatus::Stalled => "stalled",
            TaskStatus::Invalid => "invalid",
            TaskStatus::Declined => "declined",
        }
        .to_string()
    }
}

#[derive(Clone, Debug)]
pub enum BoardColumn {
    Inbox,
    Soon,
    Doing,
    ClinicDuty,
    NeedsDiscussion,
    Blocked,
    Watching,
    Epic,
    Graveyard,
    DDoing,
    DToday,
    DRefined,
    DBacklog,
    DInputGiven,
    DBlocked,
    DDone,
}

impl std::str::FromStr for BoardColumn {
    type Err = Box<dyn Error>;
    fn from_str(mystr: &str) -> Result<Self, Self::Err> {
        match mystr {
            "inbox" | "i" => Some(BoardColumn::Inbox),
            "soon" | "s" => Some(BoardColumn::Soon),
            "doing" | "d" => Some(BoardColumn::Doing),
            "clinic_duty" | "clinic" | "cd" => Some(BoardColumn::ClinicDuty),
            "needs_discussion" | "discussion" | "nd" => Some(BoardColumn::NeedsDiscussion),
            "blocked" | "b" => Some(BoardColumn::Blocked),
            "watching" | "w" => Some(BoardColumn::Watching),
            "epic" | "e" => Some(BoardColumn::Epic),
            "graveyard" | "g" => Some(BoardColumn::Graveyard),
            "ddoing" | "dd" => Some(BoardColumn::DDoing),
            "dtoday" | "dt" => Some(BoardColumn::DToday),
            "drefined" | "dr" => Some(BoardColumn::DRefined),
            "dinputgiven" | "dig" => Some(BoardColumn::DInputGiven),
            "dbacklog" | "backlog" | "dtr" => Some(BoardColumn::DBacklog),
            "dblocked" | "db" => Some(BoardColumn::DBlocked),
            "ddone" => Some(BoardColumn::DDone),
            _ => None,
        }
        .ok_or(Box::new(InvalidChoice {
            message: format!("Invalid BoardColumn {}", mystr),
        }))
    }
}

impl BoardColumn {
    pub fn to_string(&self) -> String {
        match self {
            BoardColumn::Inbox => "inbox",
            BoardColumn::Soon => "soon",
            BoardColumn::Doing => "doing",
            BoardColumn::ClinicDuty => "clinic_duty",
            BoardColumn::NeedsDiscussion => "needs_discussion",
            BoardColumn::Blocked => "blocked",
            BoardColumn::Watching => "watching",
            BoardColumn::Epic => "epic",
            BoardColumn::Graveyard => "graveyard",
            BoardColumn::DDoing => "ddoing",
            BoardColumn::DToday => "dtoday",
            BoardColumn::DRefined => "drefined",
            BoardColumn::DBacklog => "dbacklog",
            BoardColumn::DInputGiven => "dinputgiven",
            BoardColumn::DBlocked => "dblocked",
            BoardColumn::DDone => "ddone",
        }
        .to_string()
    }
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Edge {
    #[serde(rename = "sourcePHID")]
    pub source_phid: String,
    pub edge_type: String,
    #[serde(rename = "destinationPHID")]
    pub destination_phid: String,
}
